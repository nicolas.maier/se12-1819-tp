/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

enum dmtimer_timers {
	DMTIMER_2,
	DMTIMER_3,
	DMTIMER_4,
	DMTIMER_5,
	DMTIMER_6,
	DMTIMER_7,
};

// Initialise le timer spécifié
void timer_init(enum dmtimer_timers timer);

// Donne la valeur du compteur du timer spécifié
uint32_t timer_get_counter(enum dmtimer_timers timer);

// Donne la fréquence en Hz du timer spécifié
uint32_t timer_get_frequency(enum dmtimer_timers timer);

#endif /* TIMER_H */
