/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */
#include <stdint.h>
#include <stdio.h>
#include "display.h"
#include "draw.h"

// Couleurs par défaut
#define BG_COLOR BLACK
#define CIRCLE_READY_COLOR YELLOW
#define CIRCLE_RELEASE_COLOR GREEN
#define TEXT_COLOR WHITE
#define REACTION_COLOR RED

// Positionnements
#define CIRCLE_POSITION_X 76
#define RELEASE_POSITION_Y 30
#define READY_POSITION_Y 10

#define CIRCLE_RADIUS 4
#define Y_MESSAGE 55
#define X_MESSAGE 8
#define BUFFER_SIZE 10

#define TXT_READY "Ready"
#define TXT_RELEASE "Release"
#define TXT_EARLY "Too early"

enum color {
	WHITE     = 0b1111111111111111,
	BLACK     = 0b0000000000000000,
	RED       = 0b1111100000000000,
	GREEN     = 0b0000011111100000,
	YELLOW    = 0b1111111111100000,
};

// public methods implementation -----------------------------------------------------------------

void display_init(){
		draw_init();
		draw_rect(0, 0, DISPLAY_SIZE, DISPLAY_SIZE, BG_COLOR);

		display_show_ready(false);
		display_show_release(false);
}

void display_clear_text(){
	draw_rect(X_MESSAGE, Y_MESSAGE, DISPLAY_SIZE-X_MESSAGE, CHAR_SIZE, BG_COLOR);
}

void display_show_time(uint32_t time){
	char buffer[BUFFER_SIZE];
	sprintf(buffer, "%d ms", (int)time);
	draw_string(X_MESSAGE, Y_MESSAGE, buffer, REACTION_COLOR, BG_COLOR);
}

void display_show_early(){
	draw_string(X_MESSAGE, Y_MESSAGE, TXT_EARLY, REACTION_COLOR, BG_COLOR);
}

void display_show_ready(bool color){
	draw_string(X_MESSAGE,READY_POSITION_Y, TXT_READY, color ? CIRCLE_READY_COLOR : TEXT_COLOR, BG_COLOR);
	draw_circle(CIRCLE_POSITION_X,READY_POSITION_Y+CHAR_SIZE/2,CIRCLE_RADIUS, color ? CIRCLE_READY_COLOR : TEXT_COLOR);
}

void display_show_release(bool color){
	draw_string(X_MESSAGE,RELEASE_POSITION_Y, TXT_RELEASE, color ? CIRCLE_RELEASE_COLOR : TEXT_COLOR, BG_COLOR);
	draw_circle(CIRCLE_POSITION_X,RELEASE_POSITION_Y+CHAR_SIZE/2, CIRCLE_RADIUS, color ? CIRCLE_RELEASE_COLOR : TEXT_COLOR);
}
