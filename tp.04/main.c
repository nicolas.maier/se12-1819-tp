/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Purpose:		Simple speed game using buttons, screen and timers
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "draw.h"
#include "buttons.h"
#include "timer.h"
#include "display.h"

// Bornes pour la durée aléatoire
#define MIN_DURATION_MS 500
#define MAX_DURATION_MS 2500

// Différents états du jeu
enum states {IDLE, READY, ENDED};

// local variables -----------------------------------------------------------------

static uint32_t start_time;    // Valeur du compteur au début
static uint32_t duration_time; // Durée aléatoire

// local methods implementation -----------------------------------------------------------------

static enum states idle_state(enum buttons_states b_state) {
	if (b_state == BUTTONS_PRESSED) {
		// Transition vers "READY"

		display_show_ready(true);
		display_clear_text();

		int random_ms = rand() % (MAX_DURATION_MS - MIN_DURATION_MS + 1) + MIN_DURATION_MS;
		duration_time = random_ms * (timer_get_frequency(DMTIMER_2) / 1000);
		start_time = timer_get_counter(DMTIMER_2);

		return READY;
	}
	return IDLE;
}

static enum states ready_state(enum buttons_states b_state) {
	(void) b_state;
	uint32_t curr_time = timer_get_counter(DMTIMER_2);
	if (curr_time - start_time >= duration_time) {
		// Transition vers "ENDED"

		display_show_ready(false);
		display_show_release(true);

		start_time = curr_time;

		return ENDED;
	} else if (b_state == BUTTONS_RELEASED) {
		// Transition vers "IDLE"

		display_show_ready(false);
		display_show_early();

		return IDLE;
	}
	return READY;
}

static enum states ended_state(enum buttons_states b_state) {
	(void) b_state;
	if (b_state == BUTTONS_RELEASED) {
		// Transition vers "IDLE"

		uint32_t curr_time = timer_get_counter(DMTIMER_2);
		uint32_t elapsed_time_ms = (curr_time - start_time) / (timer_get_frequency(DMTIMER_2) / 1000);

		display_show_release(false);
		display_show_time(elapsed_time_ms);

		return IDLE;
	}
	return ENDED;
}

// Tableau contenant les fonctions permettant de traiter les différents états du jeu
enum states (*handle_state[])(enum buttons_states) = {
	[IDLE] = idle_state,
	[READY] = ready_state,
	[ENDED] = ended_state,
};

int main()
{
	draw_init();
	timer_init(DMTIMER_2);
	buttons_init();
	display_init();

	enum states state = IDLE;
	while (true) {
		state = handle_state[state](buttons_get_state());
	}
	return 0;
}
