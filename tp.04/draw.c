/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "draw.h"
#include "oled.h"
#include "font_8x8.h"

// public methods implementation ---------------------------------------------------

void draw_init() {
	oled_init(OLED_V101);
}

void draw_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint16_t color) {
	oled_memory_size(DISPLAY_SIZE - y - h, DISPLAY_SIZE - 1 - y, DISPLAY_SIZE - x - w, DISPLAY_SIZE - 1 - x);
	for (int i = w*h; i > 0; i--)
		oled_color(color);
}

void draw_circle(uint8_t cx, uint8_t cy, uint8_t r, uint16_t color) {
	for (int i = -r; i <= r; i++) {
		for (int j = -r; j <= r; j++) {
			int w = round(sqrt(i*i + j*j));
			if (w <= r) {
				draw_rect(cx + i, cy + j, 1, 1, color);
			}
		}
	}
}

void draw_char(uint8_t x, uint8_t y, uint8_t c, uint16_t color, uint16_t bg_color) {
	oled_memory_size(DISPLAY_SIZE - y - CHAR_SIZE, DISPLAY_SIZE - 1 - y, DISPLAY_SIZE - x - CHAR_SIZE, DISPLAY_SIZE - 1 - x);
	for (int col = 0; col < CHAR_SIZE; col++) {
		for (int row = CHAR_SIZE; row > 0; row--) {
			oled_color(fontdata_8x8[c][row-1] & 1<<col ? color : bg_color);
		}
	}
}

void draw_number(uint8_t x, uint8_t y, int8_t number, uint16_t color, uint16_t bg_color) {
	if (number < 0) {
		number = -number;
		draw_char(x, y, '-', color, bg_color);
	} else {
		draw_char(x, y, ' ', color, bg_color);
	}
	if (number > 99) number = 99;
	x += CHAR_SIZE;
	draw_char(x, y, number / 10 + '0', color, bg_color);
	x += CHAR_SIZE;
	draw_char(x, y, number % 10 + '0', color, bg_color);
}

void draw_string(uint8_t x, uint8_t y, char *text, uint16_t color, uint16_t bg_color) {
	for (int i = 0; text[i] != '\0'; i++) {
	    draw_char(x, y, text[i], color, bg_color);
	    x += CHAR_SIZE;
	}
}
