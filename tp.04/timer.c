/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */

#include <stdint.h>
#include <am335x_clock.h>
#include "timer.h"

#define TIOCP_CFG_SOFTRESET 0<<0
#define TCLR_ST 0<<0
#define TCLR_AR 0<<1

static const enum am335x_clock_timer_modules timer2clock[] = {
	AM335X_CLOCK_TIMER2,
	AM335X_CLOCK_TIMER3,
	AM335X_CLOCK_TIMER4,
	AM335X_CLOCK_TIMER5,
	AM335X_CLOCK_TIMER6,
	AM335X_CLOCK_TIMER7,
};

struct timer_ctrl {
	uint32_t tidr;
	uint32_t res1[3];
	uint32_t tiocp_cfg;
	uint32_t res2[3];
	uint32_t irq_eoi;
	uint32_t irqstatus_raw;
	uint32_t irqstatus;
	uint32_t irqenable_set;
	uint32_t irqenable_clr;
	uint32_t irqwakeen;
	uint32_t tclr;
	uint32_t tcrr;
	uint32_t tldr;
	uint32_t ttgr;
	uint32_t twps;
	uint32_t tmar;
	uint32_t tcar1;
	uint32_t tsicr;
	uint32_t tcar2;
};

// local variables -----------------------------------------------------------------

static volatile struct timer_ctrl* timer_ctrls[] = {
	[DMTIMER_2] = (struct timer_ctrl*)0x48040000,
	[DMTIMER_3] = (struct timer_ctrl*)0x48042000,
	[DMTIMER_4] = (struct timer_ctrl*)0x48044000,
	[DMTIMER_5] = (struct timer_ctrl*)0x48046000,
	[DMTIMER_6] = (struct timer_ctrl*)0x48048000,
	[DMTIMER_7] = (struct timer_ctrl*)0x4804A000,
};

// public methods implementation ---------------------------------------------------

void timer_init(enum dmtimer_timers timer) {
	am335x_clock_enable_timer_module(timer2clock[timer]);
	volatile struct timer_ctrl* ctrl = timer_ctrls[timer];

	ctrl->tiocp_cfg |= TIOCP_CFG_SOFTRESET; // force le reset software
	while ((ctrl->tiocp_cfg &= TIOCP_CFG_SOFTRESET) != 0);

	ctrl->tldr = 0;
	ctrl->tcrr = 0;
	ctrl->ttgr = 0;
	ctrl-> tclr |= TCLR_AR | TCLR_ST;
}

uint32_t timer_get_counter(enum dmtimer_timers timer) {
	return timer_ctrls[timer]->tcrr;
}

uint32_t timer_get_frequency(enum dmtimer_timers timer) {
	(void)timer; // permet d'enlever le warning "paramètre timer pas utilisé"
	return 24000000;
}
