/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "buttons.h"

#define SW_GPIO AM335X_GPIO1
#define S1_PIN 15

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// local variables -----------------------------------------------------------------

static uint8_t former_state = 0;

static const enum buttons_states buttons_states[2][2] = {
	{BUTTONS_OPEN, BUTTONS_PRESSED},
	{BUTTONS_RELEASED, BUTTONS_CLOSED},
};

// public methods implementation ---------------------------------------------------

void buttons_init()
{
	// initialize gpio module
	am335x_gpio_init(SW_GPIO);

	// configure gpio pin as input
	am335x_gpio_setup_pin_in(SW_GPIO, S1_PIN, AM335X_GPIO_PULL_NONE, false);
}

enum buttons_states buttons_get_state()
{
	uint32_t state = ((1<<S1_PIN) & am335x_gpio_get_states(SW_GPIO)) == 0 ? 1 : 0;
	enum buttons_states transition = buttons_states[former_state][state];
	former_state = state;
	return transition;
}
