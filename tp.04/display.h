/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		04.12.2018
 */
#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H
#include <stdbool.h>

// Initialise les composants pour l'écran
void display_init();

// Affiche les différents éléments sur l'écran
void display_show_time(uint32_t time);
void display_show_early();
void display_clear_text();
void display_show_ready(bool color);
void display_show_release(bool color);

#endif
