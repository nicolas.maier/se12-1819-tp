/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "display.h"
#include "draw.h"
#include "oled.h"
#include "font_8x8.h"

// Positions/tailles des éléments à dessiner
#define Y_OFFSET_GRAD 10
#define Y_SPACE_GRAD 15
#define X_OFFSET_GRAD 28
#define W_GRAD_BAR 6
#define H_GRAD_BAR 1
#define H_HALF_CHAR 3
#define THERMO_X_CENTER 39
#define THERMO_BAR_WIDTH 11
#define THERMO_BAR_HEIGHT 55
#define THERMO_INSIDE_BAR_HEIGHT 65
#define THERMO_CIRCLE_RAD 12
#define THERMO_BORDER_WIDTH 4
#define THERMO_CONST_BLUE_BAR_HEIGHT 15
#define THERMO_Y_START 9
#define THERMO_Y_STOP 59
#define THERMO_Y_CIRCLE 74
#define X_OFFSET_LABELS 54
#define Y_OFFSET_LABELS 10
// Position min/max des curseurs des températures
#define MIN_PIXEL 58
#define MAX_PIXEL 13
// Valeurs min et max affichables
#define MIN_TEMP -20
#define MAX_TEMP 40

// Couleurs par défaut
#define DEFAULT_BG_COLOR BLACK
#define DEFAULT_THERMO_COLOR WHITE
#define DEFAULT_MAX_COLOR RED
#define DEFAULT_MIN_COLOR BLUE
#define DEFAULT_CURR_COLOR GRAY

// local variables -----------------------------------------------------------------

enum color {
	WHITE     = 0b1111111111111111,
	BLACK     = 0b0000000000000000,
	RED       = 0b1111100000000000,
	GREEN     = 0b1111111111100000,
	BLUE      = 0b0011100011111111,
	GRAY      = 0b1011010110110110
};

static enum color bg_color = DEFAULT_BG_COLOR;
static enum color thermo_color = DEFAULT_THERMO_COLOR;
static enum color max_color = DEFAULT_MAX_COLOR;
static enum color min_color = DEFAULT_MIN_COLOR;
static enum color curr_color = DEFAULT_CURR_COLOR;

// private methods implementation ---------------------------------------------------

// Mappe une valeur de température vers le pixel correspondant
static uint8_t display_temp_to_pos(int8_t temp) {
	double slope = 1.0 * (MAX_PIXEL - MIN_PIXEL) / (MAX_TEMP - MIN_TEMP);
	return MIN_PIXEL + round(slope * (temp - MIN_TEMP));
}

// Rend la température donnée en s'assurant qu'elle est bien entre MIN_TEMP et MAX_TEMP
static int8_t display_norm_temp(int8_t temp) {
	if (temp < MIN_TEMP) return MIN_TEMP;
	if (temp > MAX_TEMP) return MAX_TEMP;
	return temp;
}

static void display_render_elements() {
	// Fond
	draw_rect(0, 0, DISPLAY_SIZE, DISPLAY_SIZE, bg_color);

	// Nombres pour la graduation
	draw_char(CHAR_SIZE*1, Y_OFFSET_GRAD + Y_SPACE_GRAD*0, '4', thermo_color, bg_color);
	draw_char(CHAR_SIZE*2, Y_OFFSET_GRAD + Y_SPACE_GRAD*0, '0', thermo_color, bg_color);

	draw_char(CHAR_SIZE*1, Y_OFFSET_GRAD + Y_SPACE_GRAD*1, '2', thermo_color, bg_color);
	draw_char(CHAR_SIZE*2, Y_OFFSET_GRAD + Y_SPACE_GRAD*1, '0', thermo_color, bg_color);

	draw_char(CHAR_SIZE*2, Y_OFFSET_GRAD + Y_SPACE_GRAD*2, '0', thermo_color, bg_color);

	draw_char(CHAR_SIZE*0, Y_OFFSET_GRAD + Y_SPACE_GRAD*3, '-', thermo_color, bg_color);
	draw_char(CHAR_SIZE*1, Y_OFFSET_GRAD + Y_SPACE_GRAD*3, '2', thermo_color, bg_color);
	draw_char(CHAR_SIZE*2, Y_OFFSET_GRAD + Y_SPACE_GRAD*3, '0', thermo_color, bg_color);

	// Gradutation
	draw_rect(X_OFFSET_GRAD, Y_OFFSET_GRAD + Y_SPACE_GRAD*0 + H_HALF_CHAR, W_GRAD_BAR, H_GRAD_BAR, thermo_color);
	draw_rect(X_OFFSET_GRAD, Y_OFFSET_GRAD + Y_SPACE_GRAD*1 + H_HALF_CHAR, W_GRAD_BAR, H_GRAD_BAR, thermo_color);
	draw_rect(X_OFFSET_GRAD, Y_OFFSET_GRAD + Y_SPACE_GRAD*2 + H_HALF_CHAR, W_GRAD_BAR, H_GRAD_BAR, thermo_color);
	draw_rect(X_OFFSET_GRAD, Y_OFFSET_GRAD + Y_SPACE_GRAD*3 + H_HALF_CHAR, W_GRAD_BAR, H_GRAD_BAR, thermo_color);

	// Forme extérieure du thermomètre
	draw_circle(THERMO_X_CENTER, THERMO_Y_CIRCLE, THERMO_CIRCLE_RAD, thermo_color);
	draw_rect(THERMO_X_CENTER - THERMO_BORDER_WIDTH - (THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH) / 2, THERMO_Y_START, THERMO_BAR_WIDTH, THERMO_BAR_HEIGHT, thermo_color);
	draw_circle(THERMO_X_CENTER, THERMO_Y_START, THERMO_BAR_WIDTH/2, thermo_color);

	// Partie creusée du thermomètre
	draw_rect(THERMO_X_CENTER - (THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH) / 2, THERMO_Y_START, THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH, THERMO_INSIDE_BAR_HEIGHT, bg_color);

	// Partie du thermomètre qui reste tout le temps bleue
	draw_circle(THERMO_X_CENTER, THERMO_Y_CIRCLE, THERMO_CIRCLE_RAD - THERMO_BORDER_WIDTH, min_color);
	draw_rect(THERMO_X_CENTER - (THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH) / 2, THERMO_Y_STOP, THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH, THERMO_CONST_BLUE_BAR_HEIGHT, min_color);

	// Labels pour la partie texte
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*0, Y_OFFSET_LABELS + CHAR_SIZE*0, 'M', max_color  , bg_color);
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*1, Y_OFFSET_LABELS + CHAR_SIZE*0, '=', max_color  , bg_color);
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*0, Y_OFFSET_LABELS + CHAR_SIZE*1, 'c', curr_color, bg_color);
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*1, Y_OFFSET_LABELS + CHAR_SIZE*1, '=', curr_color, bg_color);
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*0, Y_OFFSET_LABELS + CHAR_SIZE*2, 'm', min_color , bg_color);
	draw_char(X_OFFSET_LABELS + CHAR_SIZE*1, Y_OFFSET_LABELS + CHAR_SIZE*2, '=', min_color , bg_color);
}

// public methods implementation ---------------------------------------------------

void display_init() {
	draw_init();

	display_render_elements();
}

void display_show_temp(int8_t curr, int8_t min, int8_t max) {
	draw_number(X_OFFSET_LABELS + CHAR_SIZE*2, Y_OFFSET_LABELS + CHAR_SIZE*0, max, max_color, bg_color);
	draw_number(X_OFFSET_LABELS + CHAR_SIZE*2, Y_OFFSET_LABELS + CHAR_SIZE*1, curr, curr_color, bg_color);
	draw_number(X_OFFSET_LABELS + CHAR_SIZE*2, Y_OFFSET_LABELS + CHAR_SIZE*2, min, min_color, bg_color);

	uint8_t currPos = display_temp_to_pos(display_norm_temp(curr));
	uint8_t minPos = display_temp_to_pos(display_norm_temp(min));
	uint8_t maxPos = display_temp_to_pos(display_norm_temp(max));

	uint8_t x_pos = THERMO_X_CENTER - (THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH) / 2;
	uint8_t width = THERMO_BAR_WIDTH - 2*THERMO_BORDER_WIDTH;

	draw_rect(x_pos, MAX_PIXEL, width, maxPos-13, bg_color);
	draw_rect(x_pos, maxPos, width, currPos-maxPos, max_color);
	draw_rect(x_pos, currPos, width, minPos-currPos, curr_color);
	draw_rect(x_pos, minPos, width, MIN_PIXEL+1-minPos, min_color);
}

void display_set_thermo_color(uint16_t color) {
	thermo_color = color;
	display_render_elements();
}

void display_set_max_color(uint16_t color) {
	max_color = color;
	display_render_elements();
}

void display_set_min_color(uint16_t color) {
	min_color = color;
	display_render_elements();
}

void display_set_curr_color(uint16_t color) {
	curr_color = color;
	display_render_elements();
}

void display_reset_colors() {
	thermo_color = DEFAULT_THERMO_COLOR;
	max_color = DEFAULT_MAX_COLOR;
	min_color = DEFAULT_MIN_COLOR;
	curr_color = DEFAULT_CURR_COLOR;
	display_render_elements();
}
