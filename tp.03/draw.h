/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#ifndef DRAW_H
#define DRAW_H

#include <stdint.h>

#define DISPLAY_SIZE 96
#define CHAR_SIZE 8

// Initialise les composants pour l'écran
void draw_init();

// Dessine un rectangle en x;y, de taille w par h
void draw_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint16_t color);

// Dessine un cercle de centre cx;cy et de rayon r
void draw_circle(uint8_t cx, uint8_t cy, uint8_t r, uint16_t color);

// Dessine un caractère (8x8) en x;y
void draw_char(uint8_t x, uint8_t y, uint8_t c, uint16_t color, uint16_t bg_color);

// Dessine un nombre entre -99 et 99 en x;y
void draw_number(uint8_t x, uint8_t y, int8_t number, uint16_t color, uint16_t bg_color);

#endif /* DRAW_H */
