/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#include <am335x_i2c.h>
#include <stdbool.h>
#include "thermo.h"

#define THERMO 0x48
#define TEMP 0
#define CONFIG 1
#define T_LOW 2
#define T_HIGH 3

// local variables -----------------------------------------------------------------

static int8_t curr;
static int8_t min;
static int8_t max;
static bool first = true;

// public methods implementation ---------------------------------------------------

void thermo_init() {
	am335x_i2c_init(AM335X_I2C2, 400000);
}

void thermo_update_temp() {
	uint8_t data[2] = {0x80, 0};
	int status = am335x_i2c_read(AM335X_I2C2, THERMO, TEMP, data, 2);
	if (status == 0) {
		curr = (int8_t)data[0];
		if (first || curr < min) min = curr;
		if (first || curr > max) max = curr;
		first = false;
	}
}

int8_t thermo_get_curr() {
	return curr;
}

int8_t thermo_get_min() {
	return min;
}

int8_t thermo_get_max() {
	return max;
}

void thermo_reset_min_max() {
	first = true;
}
