/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>

// Initialise les composants pour l'écran
void display_init();

// Affiche les températures sur le thermomètre analogique et en texte
void display_show_temp(int8_t curr, int8_t min, int8_t max);

// Change la couleur des différents éléments
void display_set_thermo_color(uint16_t color);
void display_set_max_color(uint16_t color);
void display_set_min_color(uint16_t color);
void display_set_curr_color(uint16_t color);
void display_reset_colors();

#endif /* DISPLAY_H */
