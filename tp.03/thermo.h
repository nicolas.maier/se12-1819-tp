/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#ifndef THERMO_H
#define THERMO_H

// Initialise les composants pour le thermomètre
void thermo_init();

// Mesure la température et met à jour les valeurs curr, min et max
void thermo_update_temp();

// Getters pour curr, min et max
int8_t thermo_get_curr();
int8_t thermo_get_min();
int8_t thermo_get_max();

// Réinitialise les valeurs min et max
void thermo_reset_min_max();

#endif /* THERMO_H */
