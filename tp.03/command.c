/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <am335x_console.h>
#include <stdint.h>
#include <stdbool.h>
#include "command.h"
#include "thermo.h"
#include "display.h"

#define ARRAY_SIZE(x)(sizeof(x)/sizeof(x[0]))
#define BUFFER_SIZE 100

// local variables -----------------------------------------------------------------

struct command {
	const char* name;
	void (*cmd) (int argc, char* argv[]) ;
	struct command* next;
};

static char buffer[BUFFER_SIZE];
static int buffer_index = 0;
static struct command* cmd_list = 0;

// private methods implementation ---------------------------------------------------

// Découpe la string pour en sortir les différents paramètres
static void tokenize (char* str, int* argc, char** argv, int sz) {
	*argc = 0;
	while (1) {
		while ((*str <= ' ') && (*str != '\0')) str++;
		if (*str == '\0') break;

		argv[(*argc)++] = str++;
		if (*argc >= sz) break;

		while (*str > ' ') str++;
		if (*str == '\0') break;

		*str++ = '\0';
	}
}

// Ajoute une commande à la liste
static int attach (struct command* cmd) {
	int status = 0;
	struct command* e = cmd_list;
	struct command* p = 0;
	while ((e != 0) && (strcmp(e->name, cmd->name) < 0)) {
		p = e;
		e = e->next;
	}
	if (p == 0) {
		cmd->next = cmd_list;
		cmd_list = cmd;
	} else {
		cmd->next = e;
		p->next = cmd;
	}
	return status;
}

// Traite la ligne de commande donnée
static void process (const char* command_line) {
	char str[strlen(command_line)+1];
	char* argv[100] = {""};
	int   argc = 0;

	strcpy (str, command_line);
	tokenize (str, &argc, &argv[0], ARRAY_SIZE(argv));

	const struct command *cmd = cmd_list;
	while ((cmd != 0) && (strcmp (argv[0], cmd->name) != 0)) {
		cmd = cmd->next;
	}
	if (cmd != 0) {
		cmd->cmd (argc, argv);
	} else {
		printf ("error: command not found: \"%s\"\n", argv[0]);
	}
	return;
}

// Commande "color"
static void color (int argc, char* argv[]) {
	int min_arguments = 2;
	int max_arguments = 3;
	if (argc > max_arguments) {
		printf("error: too many arguments for command %s\n", argv[0]);
		return;
	}
	if (argc < min_arguments) {
		printf("error: too few arguments for command %s\n", argv[0]);
		return;
	}

	if (strcmp(argv[1], "r") == 0) {
		printf("Colors reset\n");
		display_reset_colors();
		return;
	}

	if (argc < max_arguments) {
		printf("error: too few arguments for command %s\n", argv[0]);
		return;
	}

	uint16_t color = (uint16_t)strtol(argv[2], NULL, 16);

	if (strcmp(argv[1], "M") == 0) {
		printf("Max color set\n");
		display_set_max_color(color);
		return;
	}

	if (strcmp(argv[1], "c") == 0) {
		printf("Curr color set\n");
		display_set_curr_color(color);
		return;
	}

	if (strcmp(argv[1], "m") == 0) {
		printf("Min color set\n");
		display_set_min_color(color);
		return;
	}

	if (strcmp(argv[1], "t") == 0) {
		printf("Thermo color set\n");
		display_set_thermo_color(color);
		return;
	}

	printf("error: argument 1 must be \"M\", \"c\", \"m\" or \"r\"\n");
}
static struct command cmd_color = {
	.name = "color",
	.cmd = &color
};

// Commande "temperature"
static void temperature (int argc, char* argv[]) {
	int max_arguments = 1;
	if (argc > max_arguments) {
		printf("error: too many arguments for command %s\n", argv[0]);
		return;
	}
	printf("Min/max temperatures reset\n");
	thermo_reset_min_max();
	return;
}
static struct command cmd_temperature = {
	.name = "temperature",
	.cmd = &temperature
};

// public methods implementation ---------------------------------------------------

void command_init() {
	am335x_console_init();
	attach (&cmd_color);
	attach (&cmd_temperature);
}

void command_read_and_process() {
	if (am335x_console_tstc()) {
		char c = am335x_console_getc();
		am335x_console_putc(c);

		if (c == '\b' || c == '\177') {
			if (buffer_index > 0)
				buffer_index--;
		} else if (c == '\r' || c == '\n') {
			if (buffer_index > 0) {
				buffer[buffer_index] = '\0';
				buffer_index = 0;
				process(buffer);
			}
		} else if (buffer_index < BUFFER_SIZE-1) {
			buffer[buffer_index++] = c;
		}
	}
}
