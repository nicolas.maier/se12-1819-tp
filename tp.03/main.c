/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Purpose:		Program to read thermometer and draw on screen
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		06.11.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "command.h"
#include "thermo.h"
#include "display.h"

int test_delay(unsigned n) {
	for(n *= 10; n > 0; n--)
		printf("%i", n);
	return 0;
}

int main()
{
	command_init();
	thermo_init();
	display_init();

	while (1) {
		command_read_and_process();
		thermo_update_temp();
		display_show_temp(thermo_get_curr(), thermo_get_min(), thermo_get_max());
	}

	printf("Fin\n");

	return 0;
}
