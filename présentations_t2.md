# Présentation des projets intégrés des classes T2adfg

## Format

* Présentation : 10 minutes
* Démonstration : 2 minutes
* Questions et réponses : 3 minutes
* Feedback du public : 5 minutes
* Changement de groupe et préparation : 5 minutes

## Présentation

* [Présentations "Zen"](https://www.presentationzen.com/)
* Introduction / contexte
* Présentation de l'application
* Détails techniques
    * Utilisation des "threads", des sémaphores et des "messages queues"
    * Utilisation des interruptions
    * Utilisation (ou non) des variables globales
    * Algorithmes et structures de données utilisés
    * Prise en main et utilisation du module de communication (nRF ou RFID)
* Tests effectués
* Gestion du projet / planification
* Les hauts (satisfactions) et les bas (problèmes) du projet
* Conclusion
* Auto-évaluation du projet
    * Gestion du projet
    * Qualités techniques
    * Documenter (documentation du code et rédaction des rapports)
    * Exposer (présentation finale)


## Slots du 13 juin (groupe T2f/T2g)

| Slot | Heure | Groupe                                  |
| ---: | ----- | --------------------------------------- |
|  1   | 12:45 | Casareale Axel et Cherbuin Anthony      |
|  2   | 13:10 | da Silva Marco Kevin et Reynaud Mickaël |
|  3   | 13:35 | Javet Sébastien et Rumo Célestin        |
|  4   | 14:00 | Guibert Loïc et Pittet Raphaël          |
|  5   | 14:25 | Monney Jordane et Spada Gaëtan          |
|  6   | 14:50 | Reymond Raphael et Fasel Olivier        |


## Slots du 14 juin (groupe T2a/T2d)

| Slot | Heure | Groupe                                       |
| ---: | ----- | -------------------------------------------- |
|  1   | 08:30 | Bürgisser Kevin et Rapin Quentin             |
|  2   | 08:55 | Chassot Romain et Helfer Florian             |
|  3   | 09:20 | Camuglia Lucien et Marmy Dylan               |
|  4   | 10:00 | Moullet Tobias et Raemy Mathis               |
|  5   | 10:25 | Roten Marc et Rouvinez Sven                  |
|  6   | 10:50 | Neves Joao et Vonlanthen Vincent             |
|  7   | 11:15 | Ambrosini Jonas et Fasel Yves et Gauch Marco |


## Structure du rapport (document de présentation du travail effectué)

**Date pour le rendu du code et du rapport : lundi 10.06 à 23h59**

* Introduction
* Analyse
* Spécification / Conception
    * Algorithmes
    * Structures de données
    * Pseudo-code
    * Threads
* Réalisation
    * Architecture
    * Structure du code (modules)
* Tests et validation
* Conclusion

