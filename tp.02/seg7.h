/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#pragma once
#ifndef SEG7_H
#define SEG7_H

#include <stdint.h>

enum seg7 {
	// left 7-segment display
	SEG_G_A = (1 << 1),
	SEG_G_B = (1 << 2),
	SEG_G_C = (1 << 3),
	SEG_G_D = (1 << 4),
	SEG_G_E = (1 << 5),
	SEG_G_F = (1 << 6),
	SEG_G_G = (1 << 7),
	SEG_PG_H = (1 << 8),
	SEG_PG_B = (1 << 9),

	// right 7-segment display
	SEG_D_A = (1 << 10),
	SEG_D_B = (1 << 11),
	SEG_D_C = (1 << 12),
	SEG_D_D = (1 << 13),
	SEG_D_E = (1 << 14),
	SEG_D_F = (1 << 15),
	SEG_D_G = (1 << 16),
	SEG_PD_H = (1 << 17),
	SEG_PD_B = (1 << 18),
};

// initialize GPIOs for the 7-segment displays
void seg7_init();

// shows the specified segments on the 7-segment displays
void seg7_display_seg(uint32_t positions);

// shows the specified number on the 7-segment displays
void seg7_display_number(int8_t number);

#endif
