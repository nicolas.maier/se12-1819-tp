/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#include <stdbool.h>
#include <stdint.h>
#include "counter.h"
#include "wheel.h"
#include "seg7.h"

// local variables -----------------------------------------------------------------

static int8_t count;

// public methods implementation ---------------------------------------------------

void counter_init()
{
	count = 0;
}

void counter_update()
{
	enum wheel_direction wheel_dir = wheel_get_direction();

	// update and show count depending on wheel_dir
	switch (wheel_dir) {
	case WHEEL_LEFT:
		if (count > -99)
			count--;
		break;
	case WHEEL_RIGHT:
		if (count < 99)
			count++;
		break;
	default:
		break;
	};
	seg7_display_number(count);
}
