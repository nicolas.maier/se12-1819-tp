/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#include <stdbool.h>
#include <stdint.h>
#include <am335x_gpio.h>
#include "seg7.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// pin definition for 7-segment displays access
#define SEG_A		(1<<4)
#define SEG_B		(1<<5)
#define	SEG_C 		(1<<14)
#define	SEG_D		(1<<22)
#define	SEG_E		(1<<23)
#define	SEG_F		(1<<26)
#define	SEG_G		(1<<27)
#define GPIO2 		AM335X_GPIO2
#define	GPIO0		AM335X_GPIO0
#define DIG1		(1<<2)
#define DIG2        (1<<3)
#define DP1         (1<<4)
#define DP2         (1<<5)

// local variables -----------------------------------------------------------------

// representation of number from 0 to 9 using segments
static const uint32_t numbersTab[]	= {
	SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F        , // 0
	        SEG_B | SEG_C                                , // 1
	SEG_A | SEG_B         | SEG_D | SEG_E         | SEG_G, // 2
	SEG_A | SEG_B | SEG_C | SEG_D                 | SEG_G, // 3
	        SEG_B | SEG_C                 | SEG_F | SEG_G, // 4
	SEG_A         | SEG_C | SEG_D         | SEG_F | SEG_G, // 5
	SEG_A         | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G, // 6
	SEG_A | SEG_B | SEG_C                                , // 7
	SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G, // 8
	SEG_A | SEG_B | SEG_C | SEG_D         | SEG_F | SEG_G, // 9
};

// GPIOs pin_nr to be initialized for 7-segment displays
static const struct gpio_init {
	uint32_t pin_nr;
} gpio_init[] = {
	{ 4},	// SEGA
	{ 5},	// SEGB
	{14},	// SEGC
	{22},	// SEGD
	{23},	// SEGE
	{26},	// SEGF
	{27},	// SEGG
};

// defines if the next number must be shown on the right or the left 7-segment display
static bool show_right = false;

// public methods implementation ---------------------------------------------------

void seg7_init()
{
	// initialize gpio module
	am335x_gpio_init(GPIO0);
	am335x_gpio_init(GPIO2);

	// configure gpio pins as output
	for (int i=ARRAY_SIZE(gpio_init)-1; i>=0; i--) {
		am335x_gpio_setup_pin_out(GPIO0, gpio_init[i].pin_nr, false);
	}
	am335x_gpio_setup_pin_out(GPIO2, 2, false);
	am335x_gpio_setup_pin_out(GPIO2, 3, false);
	am335x_gpio_setup_pin_out(GPIO2, 4, false);
	am335x_gpio_setup_pin_out(GPIO2, 5, false);
}

void seg7_display_seg(uint32_t positions)
{
	// turn all segments off
	am335x_gpio_change_states(GPIO2, DP1 | DP2 | DIG1 | DIG2, false);
	am335x_gpio_change_states(GPIO0, SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G, false);

	// turn requested segments on, on the correct 7-segment display (left or right)
	if (show_right) {
		am335x_gpio_change_states(
				GPIO0,
				(positions & SEG_D_A ? SEG_A : 0) |
				(positions & SEG_D_B ? SEG_B : 0) |
				(positions & SEG_D_C ? SEG_C : 0) |
				(positions & SEG_D_D ? SEG_D : 0) |
				(positions & SEG_D_E ? SEG_E : 0) |
				(positions & SEG_D_F ? SEG_F : 0) |
				(positions & SEG_D_G ? SEG_G : 0),
				true
				);
		am335x_gpio_change_states(GPIO2, DIG2, true);
	} else {
		am335x_gpio_change_states(
				GPIO0,
				(positions & SEG_G_A ? SEG_A : 0) |
				(positions & SEG_G_B ? SEG_B : 0) |
				(positions & SEG_G_C ? SEG_C : 0) |
				(positions & SEG_G_D ? SEG_D : 0) |
				(positions & SEG_G_E ? SEG_E : 0) |
				(positions & SEG_G_F ? SEG_F : 0) |
				(positions & SEG_G_G ? SEG_G : 0),
				true
				);
		am335x_gpio_change_states(GPIO2, DIG1, true);
	}

	show_right = !show_right;
}

void seg7_display_number(int8_t number)
{
	// separate number into 3 distinct variables : negative, u (units) and t (tens)
	bool negative = false;
	if (number < 0) {
		number = -number;
		negative = true;
	}
	int8_t u = number % 10;
	int8_t t = (number / 10) % 10;

	// turn all segments off
	am335x_gpio_change_states(GPIO2, DP1 | DP2 | DIG1 | DIG2, false);
	am335x_gpio_change_states(GPIO0, SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G, false);

	// turn requested segments on, on the correct 7-segment display (left or right)
	if (show_right) {
		am335x_gpio_change_states(GPIO0, numbersTab[u], true);
		am335x_gpio_change_states(GPIO2, DIG2, true);
	} else {
		am335x_gpio_change_states(GPIO0, numbersTab[t], true);
		am335x_gpio_change_states(GPIO2, DIG1 | (negative ? DP1 : 0), true);
	}

	show_right = !show_right;
}
