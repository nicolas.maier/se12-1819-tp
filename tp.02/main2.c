/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Purpose:		Program to use GPIOs to control leds, 7-segment displays, buttons and a wheel
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include "wheel.h"
#include "leds.h"
#include "buttons.h"
#include "seg7.h"
#include "serpentine.h"
#include "counter.h"

enum mode {
	MODE_COUNTER,
	MODE_SERPENTINE,
};

int main()
{
	enum mode m = MODE_COUNTER;

	// initialize GPIO modules
	wheel_init();
	leds_init();
	buttons_init();
	seg7_init();

	// initialize mode modules
	counter_init();
	serpentine_init();

	// show initial mode led
	leds_set_state(LED1, true);

	while(true) {
		uint32_t btns = buttons_get_state();

		// handle btn 1 and 2, set mode and show led 1 or 2
		if (btns & BTN1) {
			m = MODE_COUNTER;
			leds_set_state(LED1, true);
			leds_set_state(LED2, false);
		} else if (btns & BTN2) {
			m = MODE_SERPENTINE;
			leds_set_state(LED2, true);
			leds_set_state(LED1, false);
		}

		// handle btn 3 and show led 3
		if (btns & BTN3) {
			counter_init();
			serpentine_init();
			leds_set_state(LED3, true);
		} else {
			leds_set_state(LED3, false);
		}

		// update current mode
		if (m == MODE_COUNTER) {
			counter_update();
		} else {
			serpentine_update();
		}
	}

	return 0;
}
