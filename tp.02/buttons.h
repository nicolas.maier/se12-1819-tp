/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#pragma once
#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdint.h>

enum buttons {
	BTN1 = 1<<15,
	BTN2 = 1<<16,
	BTN3 = 1<<17,
};

// initialize GPIOs for the buttons
void buttons_init();

// returns the current state of buttons
uint32_t buttons_get_state();

#endif
