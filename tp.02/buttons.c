/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "buttons.h"

#define GPIO1 AM335X_GPIO1

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// local variables -----------------------------------------------------------------

static const struct gpio_init {
	uint8_t pin_nr;
} gpio_init[] = {
	{15},	// BTN1
	{16},	// BTN2
	{17},	// BTN3
};

// public methods implementation ---------------------------------------------------

void buttons_init()
{
	// initialize gpio module
	am335x_gpio_init(GPIO1);

	// configure gpio pins as input
	for (int8_t i = ARRAY_SIZE(gpio_init)-1; i >= 0; i--) {
		am335x_gpio_setup_pin_in(GPIO1, gpio_init[i].pin_nr, AM335X_GPIO_PULL_NONE, true);
	}
}

uint32_t buttons_get_state()
{
	uint32_t state = 0;
	for (int8_t i = ARRAY_SIZE(gpio_init)-1; i >= 0; i--) {
		if (am335x_gpio_get_state(GPIO1, gpio_init[i].pin_nr)) state += 1<<gpio_init[i].pin_nr;
	}
	return ~state;
}
