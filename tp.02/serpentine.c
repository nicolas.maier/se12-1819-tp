/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author: 		Nicolas Maier - Jonathan Dias Vicente
 * Date: 		02.10.2018
 */

#include <stdbool.h>
#include <stdint.h>
#include "serpentine.h"
#include "wheel.h"
#include "seg7.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define serpentine_size 2;

// local variables -----------------------------------------------------------------

// array containing the path followed by the serpentine
static const enum seg7 positions[] = {
	SEG_G_G,
	SEG_G_F,
	SEG_G_A,
	SEG_G_B,
	SEG_G_C,
	SEG_G_D,
	SEG_G_E,
	SEG_G_G,

	SEG_D_G,
	SEG_D_C,
	SEG_D_D,
	SEG_D_E,
	SEG_D_F,
	SEG_D_A,
	SEG_D_B,
	SEG_D_G,
};

// current position of the serpentine
static uint8_t position;

// local methods implementation ----------------------------------------------------

// real modulo operation (not the same as % ("remainder") operator)
static int mod(int a, int b)
{
    int r = a % b;
    return r < 0 ? r + b : r;
}

// public methods implementation ---------------------------------------------------

void serpentine_init()
{
	position = 0;
}

void serpentine_update()
{
	enum wheel_direction wheel_dir = wheel_get_direction();

	// update serpentine position depending on wheel_dir
	switch (wheel_dir) {
	case WHEEL_LEFT:
		position--;
		break;
	case WHEEL_RIGHT:
		position++;
		break;
	default:
		break;
	};
	position = mod(position, ARRAY_SIZE(positions));

	// build a serpentine of the given size
	uint32_t display_seg = 0;
	int size_count = serpentine_size;
	while (size_count-- > 0) {
		display_seg |= positions[mod(position + size_count, ARRAY_SIZE(positions))];
	}

	// and show it
	seg7_display_seg(display_seg);
}
