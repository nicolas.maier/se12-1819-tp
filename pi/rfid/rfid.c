/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * This program is mainly based on the sample code developped by
 * Mikroelektronika to get access to the orignal code go to:
 * https://libstock.mikroe.com/projects/view/739/rfid-click-example
 */

#include "rfid.h"
#include <am335x_dmtimer1.h>
#include <am335x_gpio.h>
#include <am335x_spi.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// CR95HF Commands Definition
#define IDN 0x01
#define ProtocolSelect 0x02
#define SendRecv 0x04
#define RdReg 0x08
#define WrReg 0x09
#define ECHO 0x55

#define RETCODE_OK 0x80

// CAPE1
#define SSI_0 10
#define SSI_1 8
#define SSI0_GPIO AM335X_GPIO0
#define SSI1_GPIO AM335X_GPIO0
#define IRQ_IN 18
#define IRQ_GPIO AM335X_GPIO1

#define SPI_CTRL AM335X_SPI1
#define SPI_CHAN AM335X_CHAN0
#define SPI_FREQ 2000000
#define SPI_WORD 8
#define SPI_NOP 2

#define delay_us(us) am335x_dmtimer1_wait_us(us)

static inline void send_data(const uint8_t* val, size_t nb)
{
    am335x_spi_write_b(SPI_CTRL, SPI_CHAN, val, nb);
}

static inline void read_data(uint8_t cmd, uint8_t* buffer, size_t nb)
{
    am335x_spi_read_b(SPI_CTRL, SPI_CHAN, cmd, SPI_NOP, buffer, nb);
}

static inline bool is_ready()
{
    uint8_t buffer[2];
    read_data(0x03, buffer, 2);
    return (buffer[1] & 0x08) != 0;
}

static ssize_t send_cmd(const uint8_t* cmd,
                        size_t cmd_sz,
                        uint8_t* resp,
                        size_t resp_sz)
{
    send_data(cmd, cmd_sz);

    while (!is_ready()) {
    }

    uint8_t buffer[resp_sz + 3];
    read_data(0x02, buffer, sizeof(buffer));

    uint8_t retcode = buffer[1];
    uint8_t len     = buffer[2] > resp_sz ? resp_sz : buffer[2];

    if ((retcode == RETCODE_OK) || (retcode == 0)) {
        if (len > 0) memcpy(resp, &buffer[3], len);
        return len;
    }
    return -retcode;
}

// Get Echo reponse from CR95HF
static bool rfid_echo_response()
{
    uint8_t cmd[] = {0, 0x55};
    send_data(cmd, 2);

    while (1) {
        if (is_ready()) {
            uint8_t buffer[2];
            read_data(0x02, buffer, sizeof(buffer));
            uint8_t echo = buffer[1];
            return (echo == ECHO);
        }
    }
}

static inline void rfid_init_chip()
{
    // startup sequence
    delay_us(1000);
    am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, false);
    delay_us(1000);
    am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, true);
    delay_us(10000);

    // reset chip
    const uint8_t reset[1] = {0x1};
    send_data(reset, sizeof(reset));
    am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, false);
    delay_us(1000);
    am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, true);
    delay_us(10000);

    // wait until CR95HF is detected
    while (!rfid_echo_response()) {
        am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, true);
        delay_us(1000);
        am335x_gpio_change_state(IRQ_GPIO, IRQ_IN, false);
        delay_us(1000);
    }
}

// Select the RF communication protocol (ISO/IEC 14443-A)
static void rfid_select_iso_iec_14443_a_protocol()
{
    static const uint8_t cmd[] = {
        0,
        ProtocolSelect,
        2,
        0x02,
        0x00,
    };
    send_cmd(cmd, sizeof(cmd), 0, 0);
}

// Configure IndexMod & Gain
static void rfid_config_index_mod_gain()
{
    static const uint8_t cmd[] = {
        0,
        WrReg,
        6,
        0x09,
        0x04,
        0x68,
        0x01,
        0x01,
        0x50,
    };
    send_cmd(cmd, sizeof(cmd), 0, 0);
}

// Configure Auto FDet
static void rfid_config_auto_fdet()
{
    static const uint8_t cmd[] = {
        0,
        WrReg,
        6,
        0x09,
        0x04,
        0x0A,
        0x01,
        0x02,
        0xA1,
    };
    send_cmd(cmd, sizeof(cmd), 0, 0);
}

void init_cpu_and_peripherals()
{
    // init spi controller
    am335x_spi_init(SPI_CTRL, SPI_CHAN, SPI_FREQ, SPI_WORD);

    // Configure GPIO pins
    am335x_gpio_init(SSI0_GPIO);
    am335x_gpio_init(SSI1_GPIO);
    am335x_gpio_init(IRQ_GPIO);
    am335x_gpio_setup_pin_out(IRQ_GPIO, IRQ_IN, true);
    am335x_gpio_setup_pin_out(SSI0_GPIO, SSI_0, true);
    am335x_gpio_setup_pin_out(SSI1_GPIO, SSI_1, false);

    // init and configure cr95hf
    rfid_init_chip();
    rfid_config_index_mod_gain();
    rfid_config_auto_fdet();
    rfid_select_iso_iec_14443_a_protocol();
}

// Get CR95HF chip ID
const char* rfid_read_chip_id()
{
    static uint8_t cr95hf_id[15] = {
        [0] = 0,
    };
    static const uint8_t cmd[] = {
        0,
        IDN,
        0,
    };
    send_cmd(cmd, sizeof(cmd), cr95hf_id, sizeof(cr95hf_id) - 1);
    return (const char*)cr95hf_id;
}

// Read the tag ID
void rfid_get_tag_id(char* id)
{
    static const uint8_t cmd0[] = {
        0,
        SendRecv,
        2,
        0x26,
        0x07,
    };
    uint8_t buf[RFID_ID_LEN * 2 + 4];
    send_cmd(cmd0, sizeof(cmd0), buf, sizeof(buf));

    static const uint8_t cmd1[] = {
        0,
        SendRecv,
        3,
        0x93,
        0x20,
        0x08,
    };
    ssize_t len = send_cmd(cmd1, sizeof(cmd1), buf, sizeof(buf));

    id[0] = 0;
    if (len == 8) {
        for (int j = 0; j < len; j++) {
            char tmp[5] = "";
            sprintf(tmp, "%02x", buf[j]);
            strcat(id, tmp);
        }
    }
}
