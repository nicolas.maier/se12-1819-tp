/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       08.05.2019
 */

#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "game.h"
#include "wheel.h"
#include "communication.h"
#include "timer.h"
#include "display.h"
#include "draw.h"
#include "buttons.h"
#include "leds.h"
#include "msgq.h"
#include "sema.h"
#include "thread.h"
#include "menu.h"

// IDs pour les coups spéciaux
#define SPECIAL_NONE -1
#define SPECIAL_SHRINK 0
#define SPECIAL_QUICKBALL 1
#define SPECIAL_RANDOMBALL 2

// Noms des timers utilisés
#define TIMER_WAIT_REFRESH DMTIMER_2
#define TIMER_WAIT_READY DMTIMER_3
#define TIMER_WAIT_END DMTIMER_4

// Constantes du jeu
#define GAME_TOPBOTTOM_MARGIN 4
#define GAME_BALL_DEFAULT_SPEED 0.1
#define GAME_BALL_QUICK_SPEED 0.2
#define TEXT_POS_Y 33
#define REFRESH_MS 16 // 60 fps
#define READY_WAIT_MS 2000
#define END_WAIT_MS 5000
#define BAR_SPEED 2
#define BALL_START_OFFSET 10
#define STEP_COUNT_MAX 3
#define WIN_SCORE 5
#define MIN_ANGLE 0.4
#define PI 3.14159265
#define ANGLE_MODIFIER 0.05
#define MIN_OFFSET_ANGLE_MODIFIER 2
#define SPECIAL_LOADING_MAX 12000000
#define MAX_PSEUDORAND 1000
#define WAIT_COUNT_DISCONNECT 60

// Représente un vecteur dans le plan
struct vector {
    float x;
    float y;
};

// Représente l'état du jeu
struct game_state {
    int ally_pos; // Position de notre barre
    int ennemy_pos; // Position de la barre de l'ennemi
    struct vector ball_pos; // Position de la balle
    struct vector ball_dir; // Direction de la balle
    int ally_score; // Notre score
    int ennemy_score; // Score de l'ennemi
    int ally_specials[DISPLAY_SPECIAL_COUNT]; // Compteurs des att. spéciales
    bool is_ally_playing; // Indique si la balle se rapproche de notre barre
    bool is_waiting_ready; // Indique si on attend pour de lancer un échange
    bool is_waiting_end; // Indique si on attend pour lancer une partie
    int selected_special; // Attaque spéciale sélectionnée
    int current_special; // Attaque spéciale active
    int special_loading; // Remplissage de la barre de rechargement des att.
};

// Infos nécessaires au déroulement du jeu
struct game_info {
    bool isJ1; // Indique si on est joueur 1
    msgqid_t msgq; // File de messages non prioritaires
    msgqid_t msgq_pty; // File de messages prioritaires
} info;
struct game_state next_state; // État actuel du jeu
struct game_state former_state; // État précédent du jeu
int wheel_pos; // Compteur de la molette

// Retourne un vecteur unitaire pour l'angle donné
static struct vector dir_from_angle(float angle) {
    return (struct vector) {.x = cos(angle), .y = sin(angle)};
}

// Met à jour les leds pour correspondre à l'attaque spéciale sélectionnée
static void update_leds() {
    uint16_t leds = 0;
    if (next_state.selected_special == BUTTONS_1) leds |= LEDS_1;
    if (next_state.selected_special == BUTTONS_2) leds |= LEDS_2;
    if (next_state.selected_special == BUTTONS_3) leds |= LEDS_3;
    leds_set_state(leds);
}

// Prépare un nouvel échange
static void prepare_set() {
    next_state.ball_pos.x = (DISPLAY_GAME_AREA_SIZE-DISPLAY_BALL_SIZE)/2;
    next_state.ball_pos.y = (DISPLAY_GAME_AREA_SIZE-DISPLAY_BALL_SIZE)/2
            + (next_state.is_ally_playing
                    ? -BALL_START_OFFSET
                    : BALL_START_OFFSET);
    next_state.ball_dir.x = 0;
    next_state.ball_dir.y = 0;
    next_state.is_waiting_ready = true;
    next_state.selected_special = SPECIAL_NONE;
    update_leds();

    next_state.current_special = SPECIAL_NONE;

    display_ball(next_state.ball_pos.x, next_state.ball_pos.y,
            former_state.ball_pos.x, former_state.ball_pos.y,
            (uint8_t)next_state.current_special);
    former_state.ball_pos = next_state.ball_pos;

    display_show_text(" Ready? ", DRAW_CHAR_SIZE/2, TEXT_POS_Y);

    timer_get_counter(TIMER_WAIT_READY);
}

// Prépare une nouvelle partie
static void prepare_game() {
    wheel_pos = (DISPLAY_GAME_AREA_SIZE-DISPLAY_BAR_LENGTH)/2;
    wheel_pos -= wheel_pos % 2;
    next_state = (struct game_state) {
        .ally_pos = wheel_pos,
        .ennemy_pos = wheel_pos,
        .ball_pos = {
            .x = (DISPLAY_GAME_AREA_SIZE-DISPLAY_BALL_SIZE)/2,
            .y = (DISPLAY_GAME_AREA_SIZE-DISPLAY_BALL_SIZE)/2,
        },
        .ball_dir = (struct vector) {.x = 0.0, .y = 0.0},
        .ally_score = 0,
        .ennemy_score = 0,
        .ally_specials = {GAME_MAX_SPECIAL,GAME_MAX_SPECIAL,GAME_MAX_SPECIAL},
        .is_ally_playing = info.isJ1,
        .is_waiting_ready = true,
        .is_waiting_end = false,
        .selected_special = SPECIAL_NONE,
        .special_loading = 0,
    };
    display_clear_game_area();
    prepare_set();
    former_state = next_state;

    display_update_score_self(next_state.ally_score);
    display_update_score_ennemy(next_state.ennemy_score);
    for (int i = 0; i < DISPLAY_SPECIAL_COUNT; i++) {
        display_update_special_counter(next_state.ally_specials[i], i);
    }
    timer_get_counter(TIMER_WAIT_REFRESH);
}

// Affiche le message de fin de partie
static void show_end(bool isWin) {
    next_state.is_waiting_end = true;

    next_state.selected_special = SPECIAL_NONE;
    update_leds();

    display_clear_game_area();

    if (isWin) {
        display_show_text("You win!", DRAW_CHAR_SIZE/2, TEXT_POS_Y);
    } else {
        display_show_text("You lose!", 0, TEXT_POS_Y);
    }
    timer_get_counter(TIMER_WAIT_END);
}

// Thread principal du jeu, gère le déroulement du jeu
static void game_thread(void* param)
{
    (void)param;

    int step_count = STEP_COUNT_MAX; // Éviter d'envoyer trop de messages
    bool should_reset = false; // Indique si on doit retourner au menu
    int wait_count = 0; // Indique si on est déconnecté

    while (true) {

        if (should_reset) {
            // Retour au menu
            menu_init(info.msgq, info.msgq_pty);
        }
        should_reset = false;

        // ----------- Lecture des messages

        while (true) {
            // Lire les messages des deux files
            void* msg = msgq_fetch(info.msgq);
            if (msg == 0) msg = msgq_fetch(info.msgq_pty);
            if (msg == 0) break;
            uint8_t* msg_data = (uint8_t*) msg;

            printf("Game msg: %d\n", msg_data[0]);
            switch (msg_data[0]) {
            case COMMUNICATION_BAR:
                // Position de la barre ennemie
                next_state.ennemy_pos = msg_data[2];
                break;
            case COMMUNICATION_BALL:
                // Position et direction de la balle
                next_state.ball_pos.x = msg_data[2];
                next_state.ball_pos.y = DISPLAY_GAME_AREA_SIZE
                        - DISPLAY_BALL_SIZE - msg_data[3];
                next_state.ball_dir.x = ((float*)(msg_data + 4))[0];
                next_state.ball_dir.y = -((float*)(msg_data + 4))[1];
                timer_get_counter(TIMER_WAIT_REFRESH);
                break;
            case COMMUNICATION_BOUNCE:
                // Rebond sur la barre ennemie
                next_state.is_ally_playing = true;
                next_state.current_special = msg_data[2];

                wait_count = 0;
                display_update_disconnected(false);

                step_count = 1;
                timer_get_counter(TIMER_WAIT_REFRESH);
                break;
            case COMMUNICATION_POINT:
                // Point gagné
                next_state.is_ally_playing = true;
                next_state.ally_score += 1;
                display_update_score_self(next_state.ally_score);
                if (next_state.ally_score >= WIN_SCORE) {
                    show_end(true);
                } else {
                    prepare_set();
                }

                wait_count = 0;
                display_update_disconnected(false);

                step_count = 1;
                timer_get_counter(TIMER_WAIT_REFRESH);
                break;
            }

            free(msg);
        }

        // ------------ Traitement d'une étape du jeu

        // Attente avant de démarrer une nouvelle partie
        if (next_state.is_waiting_end) {
            uint32_t elapsed_time_4 = timer_get_time_elapsed(TIMER_WAIT_END)
                    / (timer_get_frequency(TIMER_WAIT_END) / 1000);
            if (elapsed_time_4 >= END_WAIT_MS) {
                printf("Game end (elapsed %lu)\n", elapsed_time_4);
                display_show_text("         ", 0, TEXT_POS_Y);
                prepare_game();
            }
        }

        // Attente avant de démarrer un nouvel échange
        if (next_state.is_waiting_ready) {
            uint32_t elapsed_time_3 = timer_get_time_elapsed(TIMER_WAIT_READY)
                    / (timer_get_frequency(TIMER_WAIT_READY) / 1000);
            if (elapsed_time_3 >= READY_WAIT_MS) {
                printf("Game ready (elapsed %lu)\n", elapsed_time_3);
                next_state.ball_dir.x = 0;
                next_state.ball_dir.y = next_state.is_ally_playing ? 1 : -1;
                next_state.is_waiting_ready = false;
                display_show_text("         ", 0, TEXT_POS_Y);
            }
        }

        // Attente entre le rafraichissement du jeu
        uint32_t elapsed_time_2_us = timer_get_time_elapsed(TIMER_WAIT_REFRESH)
                / (timer_get_frequency(TIMER_WAIT_REFRESH) / 1000000);
        uint32_t elapsed_time_2 = elapsed_time_2_us / 1000;
        if (elapsed_time_2 >= REFRESH_MS && !next_state.is_waiting_end) {
            timer_get_counter(TIMER_WAIT_REFRESH);
            printf("Game refresh (elapsed %lu)\n", elapsed_time_2);
            step_count--;

            update_leds();

            // Mise à jour de la barre de chargement des attaques spéciales
            next_state.special_loading += elapsed_time_2_us;
            if (next_state.special_loading > SPECIAL_LOADING_MAX) {
                next_state.special_loading = 0;
                int min_count = GAME_MAX_SPECIAL;
                int min_id = -1;
                for (int i = 0; i < DISPLAY_SPECIAL_COUNT; i++) {
                    if (next_state.ally_specials[i] < min_count) {
                        min_count = next_state.ally_specials[i];
                        min_id = i;
                    }
                }
                if (min_count < GAME_MAX_SPECIAL) {
                    display_update_special_counter(
                            ++next_state.ally_specials[min_id], min_id);
                }
            }
            display_update_special_reload_bar(
                    next_state.special_loading*100/SPECIAL_LOADING_MAX);

            // Mise à jour de la position de notre barre
            next_state.ally_pos = wheel_pos;
            if (step_count == 0) {
                communication_sendBar(next_state.ally_pos);
            }

            // Déplacement de la balle
            float ball_speed = next_state.current_special == SPECIAL_QUICKBALL
                    ? GAME_BALL_QUICK_SPEED : GAME_BALL_DEFAULT_SPEED;
            next_state.ball_pos.x += next_state.ball_dir.x
                    * ball_speed * elapsed_time_2;
            next_state.ball_pos.y += next_state.ball_dir.y
                    * ball_speed * elapsed_time_2;

            // Collisions avec les murs
            if (next_state.ball_pos.x < 0) {
                next_state.ball_pos.x = 0;
                next_state.ball_dir.x = -next_state.ball_dir.x;
            }
            if (next_state.ball_pos.x > DISPLAY_GAME_AREA_SIZE
                    - DISPLAY_BALL_SIZE) {
                next_state.ball_pos.x = DISPLAY_GAME_AREA_SIZE
                        - DISPLAY_BALL_SIZE;
                next_state.ball_dir.x = -next_state.ball_dir.x;
            }

            if (next_state.is_ally_playing) {
                // Gestion de la trajectoire de la "Random ball"
                if (next_state.current_special == SPECIAL_RANDOMBALL) {
                    if (former_state.ball_pos.y < DISPLAY_GAME_AREA_SIZE/3
                            && next_state.ball_pos.y
                            >= DISPLAY_GAME_AREA_SIZE/3) {
                        next_state.ball_dir.y = 0;

                        float angle = (1.0*rand()) / RAND_MAX;
                        angle *= (PI - 2*MIN_ANGLE);
                        angle += MIN_ANGLE;

                        next_state.ball_dir = dir_from_angle(angle);

                        if (communication_sendBall(next_state.ball_pos.x,
                                next_state.ball_pos.y, next_state.ball_dir.x,
                                next_state.ball_dir.y)) {
                            should_reset = true;
                            continue;
                        }
                    }
                }

                // Collisions avec la barre
                uint8_t round_ball_pos_x = (uint8_t) next_state.ball_pos.x;
                uint8_t round_ball_pos_y = (uint8_t) next_state.ball_pos.y;
                if (round_ball_pos_y > DISPLAY_GAME_AREA_SIZE
                        - GAME_TOPBOTTOM_MARGIN - DISPLAY_BAR_WIDTH
                        - DISPLAY_BALL_SIZE) {
                    next_state.current_special = SPECIAL_NONE;

                    // Calcul de la collision
                    int shrink = next_state.current_special == SPECIAL_SHRINK
                            ? DISPLAY_BAR_SHRINK : 0;

                    next_state.is_ally_playing = false;
                    if (round_ball_pos_x + DISPLAY_BALL_SIZE
                            >= next_state.ally_pos+shrink && round_ball_pos_x
                            <= next_state.ally_pos+DISPLAY_BAR_LENGTH-shrink) {
                        // Rebond
                        next_state.ball_pos.y = DISPLAY_GAME_AREA_SIZE
                                - GAME_TOPBOTTOM_MARGIN - DISPLAY_BAR_WIDTH
                                - DISPLAY_BALL_SIZE;

                        // Modification de l'angle en fonction de l'impact
                        int offset = (round_ball_pos_x + DISPLAY_BALL_SIZE/2)
                                - (next_state.ally_pos + DISPLAY_BAR_LENGTH/2);
                        if (offset <= MIN_OFFSET_ANGLE_MODIFIER && offset
                                >= -MIN_OFFSET_ANGLE_MODIFIER) offset = 0;
                        float angle = atan2(
                                next_state.ball_dir.y, next_state.ball_dir.x);
                        angle -= offset * ANGLE_MODIFIER;

                        if (angle < MIN_ANGLE) angle = MIN_ANGLE;
                        if (angle > PI - MIN_ANGLE) angle = PI - MIN_ANGLE;

                        next_state.ball_dir = dir_from_angle(angle);
                        next_state.ball_dir.y = -next_state.ball_dir.y;

                        // Notifier l'ennemi
                        if (communication_sendBall(next_state.ball_pos.x,
                                next_state.ball_pos.y, next_state.ball_dir.x,
                                next_state.ball_dir.y)) {
                            // Retour au menu
                            should_reset = true;
                            continue;
                        }
                        if (communication_sendEvent(COMMUNICATION_BOUNCE,
                                next_state.selected_special)) {
                            // Retour au menu
                            should_reset = true;
                            continue;
                        }

                        // Application de l'attaque spéciale sélectionnée
                        if (next_state.selected_special >= 0) {
                            display_update_special_counter(
                                    --next_state.ally_specials[
                                                 next_state.selected_special],
                                    next_state.selected_special);
                        }
                        next_state.current_special =
                                next_state.selected_special;
                        next_state.selected_special = -1;
                        update_leds();

                    } else {
                        // Point perdu
                        next_state.ennemy_score += 1;
                        display_update_score_ennemy(next_state.ennemy_score);

                        // Notifier l'ennemi
                        if (communication_sendEvent(COMMUNICATION_POINT, 0)) {
                            should_reset = true;
                            continue;
                        }

                        if (next_state.ennemy_score >= WIN_SCORE) {
                            show_end(false);
                        } else {
                            prepare_set();
                        }
                    }
                }
            } else {
                // C'est l'ennemi qui gèrera ce rebond
                if (next_state.ball_pos.y < GAME_TOPBOTTOM_MARGIN
                        + DISPLAY_BAR_WIDTH) {
                    next_state.ball_pos.y = GAME_TOPBOTTOM_MARGIN
                            + DISPLAY_BAR_WIDTH;
                    wait_count++;
                    if (wait_count >= WAIT_COUNT_DISCONNECT) {
                        // Trop de latence, on affiche l'information
                        display_update_disconnected(true);
                    }
                }
            }

            if (!next_state.is_waiting_end) {
                // Déplacement des éléments du jeu

                display_bar(next_state.ally_pos,
                    DISPLAY_GAME_AREA_SIZE-GAME_TOPBOTTOM_MARGIN
                    -DISPLAY_BAR_WIDTH,
                    former_state.ally_pos,
                    DISPLAY_GAME_AREA_SIZE-GAME_TOPBOTTOM_MARGIN
                    -DISPLAY_BAR_WIDTH, next_state.is_ally_playing
                    && next_state.current_special == SPECIAL_SHRINK);

                display_bar(next_state.ennemy_pos, GAME_TOPBOTTOM_MARGIN,
                    former_state.ennemy_pos, GAME_TOPBOTTOM_MARGIN,
                    !next_state.is_ally_playing
                    && next_state.current_special == SPECIAL_SHRINK);

                display_ball(next_state.ball_pos.x, next_state.ball_pos.y,
                        former_state.ball_pos.x, former_state.ball_pos.y,
                        (uint8_t)next_state.current_special);

                if (step_count == 0)
                    step_count = STEP_COUNT_MAX;

                // L'état actuel remplace l'état précédent
                former_state = next_state;
            }
        }

        if (menu_should_stop()) {
            // Retour au menu
            should_reset = true;
            continue;
        }

        thread_yield();
    }
}

// Handler pour les événements de la molette
static void game_wheel_handler(enum wheel_direction dir, void* param) {
    (void) param;
    printf("WHEEL %d\n", next_state.ally_pos);
    // update counter
    if (dir == WHEEL_LEFT && wheel_pos > 0)
        wheel_pos-=BAR_SPEED;
    if (dir == WHEEL_RIGHT
            && wheel_pos < DISPLAY_GAME_AREA_SIZE-DISPLAY_BAR_LENGTH)
        wheel_pos+=BAR_SPEED;
}

// Handler pour les boutons
static void game_buttons_handler(enum buttons_set btn, void* param) {
    (void) param;
    if (btn > 2) return;
    if (next_state.selected_special == btn) {
        next_state.selected_special = SPECIAL_NONE;
    } else if (!next_state.is_waiting_end && !next_state.is_waiting_ready
            && next_state.ally_specials[btn] > 0) {
        next_state.selected_special = btn;
    }
}

void game_init() {
    thread_create(game_thread, 0, "game_thread", 0);
}

void game_start(bool initIsJ1, msgqid_t msgq_game, msgqid_t msgq_game_pty) {
    // Seed de l'aléatoire basé sur un timer
    srand(timer_get_counter(TIMER_WAIT_END));

    buttons_on_event(game_buttons_handler, 0);
    wheel_on_event(game_wheel_handler, 0);

    info = (struct game_info) {
        .isJ1 = initIsJ1,
        .msgq = msgq_game,
        .msgq_pty = msgq_game_pty,
    };

    prepare_game();
}
