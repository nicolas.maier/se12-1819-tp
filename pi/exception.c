/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       27.02.2019
 */

#include <stdio.h>
#include "interrupt.h"
#include "exception.h"

// Affiche une erreur en précisant l'erreur obtenue
static void exception_handler(enum interrupt_vectors vector, void* param) {
    printf("ARM Exception(%d): %s\n", vector, (char*) param);
    while (vector == INT_PREFETCH);
}

// Initialise les différentes exceptions
void exception_init() {
    interrupt_on_event(INT_UNDEF, exception_handler, "undefined instruction");
    interrupt_on_event(INT_SVC, exception_handler, "software interrupt");
    interrupt_on_event(INT_PREFETCH, exception_handler, "prefetch abort");
    interrupt_on_event(INT_DATA, exception_handler, "data abort");
}
