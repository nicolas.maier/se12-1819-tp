/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       17.04.2019
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "communication.h"
#include "display.h"
#include "timer.h"
#include "menu.h"
#include "nrf24.h"
#include "kernel.h"
#include "thread.h"
#include "game.h"
#include "msgq.h"

// Délai entre les tentatives d'envoi
#define COMMUNICATION_RETRY_DELAY_MS 10
// Nombre d'erreurs pour redémarrer la connexion
#define COMMUNICATION_MAX_ERR_COUNT 50
// Nombre d'erreurs pour afficher le warning "déconnecté"
#define COMMUNICATION_ERR_COUNT_DISCONNECT 30

// Informations nécessaires au module de communication
struct communication_info {
    bool isJ1; // Indique si on est joueur 1
    msgqid_t msgq; // File de messages non prioritaires pour le "game"
    msgqid_t msgq_pty; // File de messages prioritaires pour le "game"

    uint8_t seq; // Numéro de séquence (ce qu'on envoie)
    uint8_t ack; // Numéro d'acquittement (ce qui a été acquité)
    uint8_t recv; // Numéro de réception (ce qu'on a reçu)
} info;

// Informations nécessaires à la connexion
typedef union mac {
    uint64_t a;
    char n[8];
} mac_t;

#define NRF_CHANNEL         115
#define NRF_TX_SIZE         12
#define NRF_ADDR_SIZE       NRF24_ADDRESS_3_BYTES
#define NRF_CRC_SIZE        NRF24_CRC_8_BIT
#define NRF_BAND_WIDTH      NRF24_DATARATE_1_MBPS
#define NRF_OUTPUT_POWER    NRF24_OUTPUT_POWER_MINUS_12_DB

static const unsigned e2dr[] = { [NRF24_DATARATE_250_KBPS] = 250,
        [NRF24_DATARATE_1_MBPS] = 1000, [NRF24_DATARATE_2_MBPS] = 2000, };

static const int e2power[] = { [NRF24_OUTPUT_POWER_ZERO_DB] = 0,
        [NRF24_OUTPUT_POWER_MINUS_6_DB] = -6, [NRF24_OUTPUT_POWER_MINUS_12_DB
                ] = -12, [NRF24_OUTPUT_POWER_MINUS_18_DB] = -18,

};

static const unsigned e2crc[] = { [NRF24_CRC_NONE] = 0, [NRF24_CRC_8_BIT] = 8,
        [NRF24_CRC_16_BIT] = 16, };

// ---------------- private methods

// Envoie un acquittement pour le numéro donné
static void send_ack(uint8_t ack_number) {
    char txdata[NRF_TX_SIZE];
    txdata[0] = COMMUNICATION_ACK;
    txdata[1] = ack_number;
    printf("sending ack %d\n", ack_number);
    int status = nrf24_write(txdata, NRF_TX_SIZE);
    if (status == -1)
        printf("error while sending data...\n");
}

// Lis un message sur chaque tunnel disponible
// et le place dans la file appropriée
// Retourne le nombre de messages lus
static int read_messages() {
    int read = 0;
    for (int p = NRF24_PIPE_P1; p <= NRF24_PIPE_P5; p++) {
        if (nrf24_readable(p)) {
            char rx_data[NRF_TX_SIZE];
            int len = nrf24_read(p, rx_data, NRF_TX_SIZE);
            if (len > 0) {
                if (len < NRF_TX_SIZE) {
                    printf("Wrong message size %d\n", len);
                }
                read++;

                if (rx_data[0] == COMMUNICATION_ACK) {
                    info.ack = rx_data[1];
                } else {
                    send_ack(rx_data[1]);
                    if (info.recv != rx_data[1]) {
                        // Création du message
                        uint8_t* msg_data = malloc(NRF_TX_SIZE);
                        memcpy(msg_data, rx_data, NRF_TX_SIZE);

                        // Ajout du message dans la file appropriée
                        if (rx_data[0] == COMMUNICATION_BAR) {
                            msgq_post(info.msgq, msg_data);
                        } else {
                            msgq_post(info.msgq_pty, msg_data);
                        }
                    }

                    // Mise à jour du numéro de réception
                    info.recv = rx_data[1];
                }
            }
        }
    }
    return read;
}

// Thread permettant de lire les messages régulièrement
static void thread_nrf_rx() {
    while (1) {
        nrf24_set_receive_mode();
        int count = 0;
        for (int i = 0; i < 10;) {
            int read = read_messages();
            if (read == 0) break;
            count += read;
        }
        thread_yield();
    }
}

// Méthode permettant au thread de dormir pendant un certain temps,
// tout en laissant les autres threads travailler
// Retourne "true" si on a été interrompu, "false" sinon
static bool sleep_ms(uint32_t ms, bool should_yield) {
    timer_get_counter(DMTIMER_5);
    while (timer_get_time_elapsed(DMTIMER_5)
            / (timer_get_frequency(DMTIMER_5) / 1000) < ms) {
        if (menu_should_stop()) {
            // Interrompu
            return true;
        }
        if (should_yield) {
            thread_yield();
        }
    }
    return false;
}

// Prépare et ouvre la connexion
static void communication_open() {
    nrf24_init();
    nrf24_set_rf_channel(NRF_CHANNEL);
    nrf24_set_rf_output_power(NRF_OUTPUT_POWER);
    nrf24_set_air_datarate(NRF_BAND_WIDTH);
    nrf24_set_crc_width(NRF_CRC_SIZE);
    nrf24_set_transfer_size(NRF_TX_SIZE);
    nrf24_enable_auto_acknowledge();
    nrf24_enable_auto_retransmit(2000, 100);

    mac_t tgt01 = { .n = "10tgt", };
    mac_t tgt02 = { .n = "20tgt", };

    if(info.isJ1){
        nrf24_set_tx_address(tgt01.a, NRF_ADDR_SIZE);
        nrf24_set_rx_address(tgt01.a, NRF_ADDR_SIZE, NRF24_PIPE_P0);
        nrf24_set_rx_address(tgt02.a, NRF_ADDR_SIZE, NRF24_PIPE_P1);
    }else{
        nrf24_set_tx_address(tgt02.a, NRF_ADDR_SIZE);
        nrf24_set_rx_address(tgt02.a, NRF_ADDR_SIZE, NRF24_PIPE_P0);
        nrf24_set_rx_address(tgt01.a, NRF_ADDR_SIZE, NRF24_PIPE_P1);
    }

    // Display the setup of the nRF24L01+ chip
    printf("nRF24L01+ Channel        : %u \n", nrf24_get_rf_channel());
    printf("nRF24L01+ Output power   : %d dBm\n",
            e2power[nrf24_get_rf_output_power()]);
    printf("nRF24L01+ Data Rate      : %u kbps\n",
            e2dr[nrf24_get_air_datarate()]);
    printf("nRF24L01+ Transfert Size : %u bytes\n", nrf24_get_transfer_size());
    printf("nRF24L01+ CRC width      : %u bits\n",
            e2crc[nrf24_get_crc_width()]);
    printf("nRF24L01+ TX Address     : 0x%010llX\n", nrf24_get_tx_address());
    printf("nRF24L01+ RX Address P0  : 0x%010llX\n",
            nrf24_get_rx_address(NRF24_PIPE_P0));
    printf("nRF24L01+ RX Address P1  : 0x%010llX\n",
            nrf24_get_rx_address(NRF24_PIPE_P1));
}

// Envoie un message et attend un acquittement de l'autre pair (bloquant)
// Retourne "true" si on a été interrompu, "false" sinon
static bool send_wait_ack(uint8_t* txdata) {
    int count = 0;
    int err_count = 0;
    while (true) {
        count++;
        int status = nrf24_write(txdata, NRF_TX_SIZE);
        if (status < 0) {
            err_count++;
            printf("Error while sending data... (error count: %d)\n",
                    err_count);

            if (err_count >= COMMUNICATION_MAX_ERR_COUNT) {
                communication_open();
                err_count = 0;
            }
        }

        if (info.ack == info.seq) {
            // Message bien acquité
            display_update_disconnected(false);
            break;
        }

        if (count >= COMMUNICATION_ERR_COUNT_DISCONNECT) {
            // Indication qu'on est déconnecté
            display_update_disconnected(true);
        }

        if (menu_should_stop()
                || sleep_ms(COMMUNICATION_RETRY_DELAY_MS, true)) {
            // Interrompu
            printf("Stop waiting ack, back to menu\n");
            return true;
        }

        thread_yield();
    }
    return false;
}

// Phase d'initialisation de la communication : envoie un message "hello" et
// attend une réponse "hello"
// Retourne "true" si on a été interrompu, "false" sinon
static bool send_wait_hello() {
    uint8_t txdata[NRF_TX_SIZE];
    txdata[0] = COMMUNICATION_HELLO;
    int err_count = 0;
    while (true) {

        if (menu_should_stop()
                || sleep_ms(COMMUNICATION_RETRY_DELAY_MS, false)) {
            // Interrompu
            printf("Stop waiting answer hello, back to menu\n");
            return true;
        }

        printf("Sending hello\n");
        int status = nrf24_write(txdata, NRF_TX_SIZE);
        if (status < 0) {
            err_count++;
            printf("Error while sending data... (error count: %d)\n",
                    err_count);

            if (err_count >= COMMUNICATION_MAX_ERR_COUNT) {
                communication_open();
                err_count = 0;
            }
        }

        bool received_hello = false;
        nrf24_set_receive_mode();
        for (int p = NRF24_PIPE_P1; p <= NRF24_PIPE_P5; p++) {
            if (nrf24_readable(p)) {
                char rx_data[NRF_TX_SIZE];
                int len = nrf24_read(p, rx_data, NRF_TX_SIZE);
                if (len > 0) {
                    if (len < NRF_TX_SIZE) {
                        printf("Wrong message size %d\n", len);
                    }

                    if (rx_data[0] == COMMUNICATION_HELLO) {
                        received_hello = true;
                    }
                }
            }
        }

        if (received_hello) {
            printf("Received answer hello\n");
            break;
        } else {
            printf("Didn't receive answer hello\n");
        }
    }

    while (nrf24_write(txdata, NRF_TX_SIZE) < 0) {
        if (menu_should_stop()) {
            // Interrompu
            return true;
        }
    }

    return false;
}

// ---------------- public methods

void communication_init() {

    thread_create(thread_nrf_rx, 0, "nrf_rx", 0);
}

bool communication_start(bool initIsJ1, msgqid_t msgq_game,
        msgqid_t msgq_game_pty) {

    info = (struct communication_info) {
        .isJ1 = initIsJ1,
        .msgq = msgq_game,
        .msgq_pty = msgq_game_pty,
        .seq = 0,
        .ack = 0,
        .recv = 0,
    };
    communication_open();
    return send_wait_hello();
}

void communication_sendBar(uint8_t position) {
    uint8_t txdata[NRF_TX_SIZE];
    txdata[0] = COMMUNICATION_BAR;
    txdata[1] = ++info.seq;
    txdata[2] = position;
    printf("sending seq %d, bar %d\n", info.seq, position);
    int status = nrf24_write(txdata, NRF_TX_SIZE);
    if (status == -1)
        printf("error while sending data...\n");
}

bool communication_sendBall(uint8_t position_x, uint8_t position_y,
        float dir_x, float dir_y) {
    uint8_t txdata[NRF_TX_SIZE];
    txdata[0] = COMMUNICATION_BALL;
    txdata[1] = ++info.seq;
    txdata[2] = position_x;
    txdata[3] = position_y;
    ((float*)(txdata + 4))[0] = dir_x;
    ((float*)(txdata + 4))[1] = dir_y;
    printf("sending seq %d, posX %d, posY %d, dirX %.6f, dirY %.6f\n",
            info.seq, position_x, position_y, dir_x, dir_y);
    return send_wait_ack(txdata);
}

bool communication_sendEvent(enum communication_message message,
        uint8_t param) {
    uint8_t txdata[NRF_TX_SIZE];
    txdata[0] = message;
    txdata[1] = ++info.seq;
    txdata[2] = param;
    printf("sending seq %d, message %d\n", info.seq, message);
    return send_wait_ack(txdata);
}
