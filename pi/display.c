/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       27.03.2019
 */

#include <stdint.h>
#include <stdbool.h>
#include <oled.h>

#include "display.h"
#include "draw.h"
#include "game.h"

// Positions/tailles des éléments à dessiner
#define GAME_AREA_MARGIN 3

#define TEXT_MARGIN 5
#define TEXT_INTER 2

#define SCORES_SEPARATOR_WIDTH 2
#define SCORES_SEPARATOR_BASE_X (GAME_AREA_SIZE + 2*GAME_AREA_MARGIN)
#define SCORES_SEPARATOR_BASE_Y (GAME_AREA_SIZE/2 + GAME_AREA_MARGIN)
#define SCORES_MARGIN 3

#define SPECIAL_LABELS_MARGIN 3
#define SPECIAL_LABELS_BASE_X SPECIAL_LABELS_MARGIN
#define SPECIAL_LABELS_BASE_Y (GAME_AREA_SIZE + 2*GAME_AREA_MARGIN\
        + SPECIAL_LABELS_MARGIN)
#define SPECIAL_LABELS_SPACING 1

#define SPECIAL_BAR_WIDTH 26
#define SPECIAL_BAR_HEIGHT 4
#define SPECIAL_BAR_BORDER_WIDTH 1
#define SPECIAL_BAR_BASE_X DRAW_DISPLAY_SIZE - SPECIAL_BAR_WIDTH - 3
#define SPECIAL_BAR_BASE_Y 87

#define GAME_AREA_SIZE DISPLAY_GAME_AREA_SIZE

// Couleurs à utiliser
#define BG_COLOR BLACK
#define ELT_COLOR WHITE
#define ERROR_COLOR ORANGE

enum color {
    WHITE     = 0b1111111111111111,
    BLACK     = 0b0000000000000000,
    RED       = 0b1111100000000000,
    GREEN     = 0b1111111111100000,
    BLUE      = 0b0011100011111111,
    YELLOW    = 0b1111111110101010,
    ORANGE    = 0b1111110111000111,
    GRAY      = 0b1011010110110110,
};

enum color special_colors[] = {BLUE, GREEN, RED};

void display_init() {
    draw_init();

    // Clear initial de l'écran
    draw_rect(0, 0, DRAW_DISPLAY_SIZE, DRAW_DISPLAY_SIZE, BG_COLOR);

    // Aire de jeu vide
    draw_rect(0, 0, GAME_AREA_SIZE + 2*GAME_AREA_MARGIN,
              GAME_AREA_SIZE + 2*GAME_AREA_MARGIN, ELT_COLOR);
    draw_rect(GAME_AREA_MARGIN, GAME_AREA_MARGIN, GAME_AREA_SIZE,
              GAME_AREA_SIZE, BG_COLOR);

    // Afficheurs des scores
    draw_rect(SCORES_SEPARATOR_BASE_X,
              SCORES_SEPARATOR_BASE_Y - SCORES_SEPARATOR_WIDTH/2,
              DRAW_DISPLAY_SIZE - SCORES_SEPARATOR_BASE_X,
              SCORES_SEPARATOR_WIDTH, ELT_COLOR);

    // Labels des attaques spéciales
    draw_char(SPECIAL_LABELS_BASE_X
              + 0*(2*DRAW_CHAR_SIZE + 2*SPECIAL_LABELS_SPACING
              + SPECIAL_LABELS_MARGIN) + DRAW_CHAR_SIZE
              + SPECIAL_LABELS_SPACING,
              SPECIAL_LABELS_BASE_Y, 'S', special_colors[0], BG_COLOR);
    draw_char(SPECIAL_LABELS_BASE_X
              + 1*(2*DRAW_CHAR_SIZE + 2*SPECIAL_LABELS_SPACING
              + SPECIAL_LABELS_MARGIN)
              + DRAW_CHAR_SIZE + SPECIAL_LABELS_SPACING,
              SPECIAL_LABELS_BASE_Y, 'Q', special_colors[1], BG_COLOR);
    draw_char(SPECIAL_LABELS_BASE_X
              + 2*(2*DRAW_CHAR_SIZE + 2*SPECIAL_LABELS_SPACING
              + SPECIAL_LABELS_MARGIN)
              + DRAW_CHAR_SIZE + SPECIAL_LABELS_SPACING,
              SPECIAL_LABELS_BASE_Y, 'R', special_colors[2], BG_COLOR);

    // Barre de rechargement des attaques spéciales
    draw_rect(SPECIAL_BAR_BASE_X - SPECIAL_BAR_BORDER_WIDTH,
              SPECIAL_BAR_BASE_Y - SPECIAL_BAR_BORDER_WIDTH,
              SPECIAL_BAR_WIDTH + 2*SPECIAL_BAR_BORDER_WIDTH,
              SPECIAL_BAR_HEIGHT + 2*SPECIAL_BAR_BORDER_WIDTH, ELT_COLOR);
    draw_rect(SPECIAL_BAR_BASE_X, SPECIAL_BAR_BASE_Y, SPECIAL_BAR_WIDTH,
              SPECIAL_BAR_HEIGHT, BG_COLOR);
}

void display_ball(uint8_t x, uint8_t y, uint8_t old_x, uint8_t old_y,
        uint8_t current_special) {
    draw_move_element(x + GAME_AREA_MARGIN, y + GAME_AREA_MARGIN,
            old_x + GAME_AREA_MARGIN, old_y + GAME_AREA_MARGIN,
            DISPLAY_BALL_SIZE, DISPLAY_BALL_SIZE,
            current_special>2 ? ELT_COLOR : special_colors[current_special],
            BG_COLOR, 0);
}

void display_bar(uint8_t x, uint8_t y,
        uint8_t old_x, uint8_t old_y, bool shrink) {
    draw_move_element(x + GAME_AREA_MARGIN, y + GAME_AREA_MARGIN,
            old_x + GAME_AREA_MARGIN, old_y + GAME_AREA_MARGIN,
            DISPLAY_BAR_LENGTH, DISPLAY_BAR_WIDTH, ELT_COLOR, BG_COLOR,
            shrink ? DISPLAY_BAR_SHRINK : 0);
}

void display_update_score_self(uint8_t score) {
    draw_char(SCORES_SEPARATOR_BASE_X + SCORES_MARGIN,
              SCORES_SEPARATOR_BASE_Y + SCORES_SEPARATOR_WIDTH/2
              + SCORES_MARGIN, score + '0', ELT_COLOR, BG_COLOR);
}

void display_update_score_ennemy(uint8_t score) {
    draw_char(SCORES_SEPARATOR_BASE_X + SCORES_MARGIN,
              SCORES_SEPARATOR_BASE_Y - SCORES_SEPARATOR_WIDTH/2
              - SCORES_MARGIN - DRAW_CHAR_SIZE,
              score + '0', ELT_COLOR, BG_COLOR);
}

void display_update_special_counter(uint8_t counter,
        enum display_specials special_id) {
    draw_char(SPECIAL_LABELS_BASE_X + special_id*(2*DRAW_CHAR_SIZE
              + 2*SPECIAL_LABELS_SPACING + SPECIAL_LABELS_MARGIN),
              SPECIAL_LABELS_BASE_Y, counter + '0',
              special_colors[special_id], BG_COLOR);
}

void display_update_special_reload_bar(uint8_t percent) {
    uint8_t length = percent * SPECIAL_BAR_WIDTH / 100 + 1;
    draw_rect(SPECIAL_BAR_BASE_X, SPECIAL_BAR_BASE_Y, length,
              SPECIAL_BAR_HEIGHT, ELT_COLOR);
    if (SPECIAL_BAR_WIDTH > length) {
        draw_rect(SPECIAL_BAR_BASE_X + length, SPECIAL_BAR_BASE_Y,
                  SPECIAL_BAR_WIDTH - length, SPECIAL_BAR_HEIGHT, BG_COLOR);
    }
}

void display_show_text(char* text, uint8_t offset_x, uint8_t offset_y) {
    int char_pos = offset_x;
    int line_pos = offset_y;
    for (int i = 0; text[i] != 0; i++) {
        if (text[i] != '\n') {
            draw_char(TEXT_MARGIN + char_pos, TEXT_MARGIN + line_pos,
                      text[i], ELT_COLOR, BG_COLOR);
            char_pos += DRAW_CHAR_SIZE;
        } else {
            char_pos = offset_x;
            line_pos += DRAW_CHAR_SIZE + TEXT_INTER;
        }
    }
}

void display_update_disconnected(bool is_disconnected) {
    static bool last_is_disconnected = false;
    if (last_is_disconnected != is_disconnected) {
        draw_char(SCORES_SEPARATOR_BASE_X + SCORES_MARGIN,
                      GAME_AREA_MARGIN + SCORES_MARGIN,
                      is_disconnected ? '!' : ' ', ERROR_COLOR, BG_COLOR);
    }
    last_is_disconnected = is_disconnected;
}

void display_clear_game_area() {
    draw_rect(GAME_AREA_MARGIN, GAME_AREA_MARGIN, GAME_AREA_SIZE,
              GAME_AREA_SIZE, BG_COLOR);
}

void display_reset_fields() {
    display_update_special_reload_bar(0);
    for (int i = 0; i < DISPLAY_SPECIAL_COUNT; i++) {
        display_update_special_counter(GAME_MAX_SPECIAL, i);
    }
    display_update_score_self(0);
    display_update_score_ennemy(0);
    display_update_disconnected(false);
}
