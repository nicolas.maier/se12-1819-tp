/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       13.03.2019
 */

#include "intc.h"
#include "interrupt.h"

// macro pour calculer le nombre d'éléments d'un tableau
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define INTC_NB_VECTORS 128

// variables -------------------------------------------

static volatile struct intc_ctrl {
    uint32_t revision;
    uint32_t res1[3];
    uint32_t sysconfig;
    uint32_t sysstatus;
    uint32_t res2[10];
    uint32_t sir_irq;
    uint32_t sir_fiq;
    uint32_t control;
    uint32_t protection;
    uint32_t idle;
    uint32_t res3[3];
    uint32_t irq_priority;
    uint32_t fiq_priority;
    uint32_t threshold;
    uint32_t res4[5];
    struct {
        uint32_t itr;
        uint32_t mir;
        uint32_t mir_clear;
        uint32_t mir_set;
        uint32_t isr_set;
        uint32_t isr_clear;
        uint32_t pending_irq;
        uint32_t pending_fiq;
    } flags[4];
    uint32_t ilr[INTC_NB_VECTORS];
}* intc = (struct intc_ctrl*) 0x48200000;

static struct listener {
    intc_service_routine_t routine;
    void* param;
} listeners[INTC_NB_VECTORS];

// private methods ---------------------------------------------------

static void intc_handler(enum interrupt_vectors i_vector_nr, void* param) {
    (void) i_vector_nr;
    (void) param; // unused arguments

    // get the irq interrupt source
    uint32_t vector_nr = intc->sir_irq & 0x7f;

    if (listeners[vector_nr].routine != 0) {
        listeners[vector_nr].routine(vector_nr, listeners[vector_nr].param);
    } else {
        // spurious interrupt
        intc->flags[vector_nr / 32].mir_set = 1 << (vector_nr % 32);
    }

    // enable processing of subsequent pending irq interrupts
    intc->control = 0x1;
}

// public methods ----------------------------------------------

void intc_init() {
    interrupt_on_event(INT_IRQ, intc_handler, 0);

    // reset intc controller
    intc->sysconfig = 0x2;
    while ((intc->sysstatus & 0x1) == 0)
        ;

    // default config, no threshold
    intc->sysconfig = 0x0;
    intc->idle = 0;
    intc->threshold = 0xff;

    // mask all interrupt request lines
    for (int i = ARRAY_SIZE(intc->flags) - 1; i >= 0; i--) {
        intc->flags[i].mir = -1;
        intc->flags[i].mir_set = -1;
        intc->flags[i].isr_clear = -1;
    }

    // set INTC priority level to 0 and select IRQ request
    for (int i = ARRAY_SIZE(intc->ilr) - 1; i >= 0; i--) {
        intc->ilr[i] = 0;
    }
}

int intc_on_event(enum intc_vectors vector_nr, intc_service_routine_t routine,
        void* param) {
    if (vector_nr >= INTC_NB_VECTORS)
        return -1;
    struct listener* listener = &listeners[vector_nr];
    if (routine == 0) {
        intc->flags[vector_nr / 32].mir_set = 1 << (vector_nr % 32);
        listener->routine = 0;
        listener->param = 0;
        return 0;
    }
    if (listener->routine == 0) {
        listener->routine = routine;
        listener->param = param;
        intc->flags[vector_nr / 32].mir_clear = 1 << (vector_nr % 32);
        return 0;
    }
    return -1;
}

void intc_force(enum intc_vectors vector_nr, bool force) {
    if (vector_nr < INTC_NB_VECTORS) {
        if (force)
            intc->flags[vector_nr / 32].isr_set = 1 << (vector_nr % 32);
        else
            intc->flags[vector_nr / 32].isr_clear = 1 << (vector_nr % 32);
    }
}
