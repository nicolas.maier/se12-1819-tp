/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       27.03.2019
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>
#include <stdbool.h>

#define DISPLAY_GAME_AREA_SIZE 76
#define DISPLAY_BAR_WIDTH 2
#define DISPLAY_BAR_LENGTH 18
#define DISPLAY_BALL_SIZE 4
#define DISPLAY_BAR_SHRINK 4

enum display_specials {
    DISPLAY_SPECIAL_SHRINK,
    DISPLAY_SPECIAL_QUICKBALL,
    DISPLAY_SPECIAL_RANDOMBALL,
    DISPLAY_SPECIAL_COUNT // must be last
};

// Initialise les composants pour l'écran
void display_init();

// Affiche/cache la balle aux coordonnées données
void display_ball(uint8_t x, uint8_t y, uint8_t old_x, uint8_t old_y,
        uint8_t current_special);

// Affiche/cache une barre aux coordonnées données
void display_bar(uint8_t x, uint8_t y, uint8_t old_x, uint8_t old_y,
        bool shrink);

// Met à jour les scores des joueurs
void display_update_score_self(uint8_t score);
void display_update_score_ennemy(uint8_t score);

// Met à jour le compteur d'une attaque spéciale
void display_update_special_counter(uint8_t counter,
        enum display_specials special_id);

// Met à jour la barre de rechargement des attaques spéciales
void display_update_special_reload_bar(uint8_t percent);

// Affiche le texte donné
void display_show_text(char* text, uint8_t offset_x, uint8_t offset_y);

// Affiche une icône indiquant un problème de connexion
void display_update_disconnected(bool is_disconnected);

// Vide la zone de jeu
void display_clear_game_area();

// Reset les champs autour du terrain
void display_reset_fields();

#endif /* DISPLAY_H */
