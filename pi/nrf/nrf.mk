EXEC=app
SRCS=$(wildcard *.c)
ASRC=$(wildcard *.S)

CFLAGS+=-I../kernel
LDFLAGS+= -L../kernel -l kernel

# Include the standard makefile for the embedded systems 1 & 2 labs 
include $(LMIBASE)/bbb/make/bbb.mk
