/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       13.03.2019
 */

#include <am335x_gpio.h>

#ifndef GPIO_H
#define GPIO_H

enum gpio_modes {
    GPIO_DEBOUNCED = (1 << 0),  ///< event generation shall be debounced
    GPIO_PULL_UP = (1 << 1),  ///< signal pull-up shall be enabled
    GPIO_RISING = (1 << 2),  ///< event generation on rising edge
    GPIO_FALLING = (1 << 3),  ///< event generation on falling edge
    GPIO_HIGH = (1 << 4),  ///< event generation on high level
    GPIO_LOW = (1 << 5),  ///< event generation on low level
};

typedef void (*gpio_service_routine_t)(void* param);

/**
 * initialization method
 * should be called prior any other method of this module
 */
extern void gpio_init();

/**
 * method to hook and unhook an gpio service routine to a specified
 * gpio interrupt source
 *
 * @param module GPIO module number
 * @param pin GPIO pin number
 * @param modes detection modes and debounced activation
 * @param routine interrupt service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int gpio_on_event(enum am335x_gpio_modules module, uint32_t pin,
        unsigned modes, gpio_service_routine_t routine, void* param);

#endif /* GPIO_H_ */
