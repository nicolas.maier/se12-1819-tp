/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       13.03.2019
 */

#include <stdbool.h>
#include <stdint.h>
#include <am335x_gpio.h>
#include "wheel.h"
#include "gpio.h"

// i/o pin definition
#define CHA_GPIO AM335X_GPIO2
#define CHA_PIN 1
#define CHB_GPIO AM335X_GPIO1
#define CHB_PIN 29

static struct listener {
    wheel_handler_t routine;
    void* param;
} listener;

// local methods implementation ---------------------------------

static void wheel_handler(void* param) {
    (void) param;
    enum wheel_direction dir = WHEEL_RIGHT;
    bool cha = am335x_gpio_get_state(CHA_GPIO, CHA_PIN);
    bool chb = am335x_gpio_get_state(CHB_GPIO, CHB_PIN);
    if (cha == chb)
        dir = WHEEL_LEFT;
    if (listener.routine != 0) {
        listener.routine(dir, listener.param);
    }
}

// public methods implementation --------------------------------

/**
 * method to initialize the resoures of the wheel
 * this method shall be called prior any other.
 */
void wheel_init() {
    // configure gpio pins as input
    am335x_gpio_setup_pin_in(CHA_GPIO, CHA_PIN, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(CHB_GPIO, CHB_PIN, AM335X_GPIO_PULL_NONE, true);

    gpio_on_event(CHA_GPIO, CHA_PIN,
            GPIO_FALLING | GPIO_RISING | GPIO_DEBOUNCED, wheel_handler, 0);
}

/**
 * method to hook and unhook a wheel service routine to a specified
 * interrupt source
 *
 * @param routine wheel service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
int wheel_on_event(wheel_handler_t routine, void* param) {
    if (routine == 0) {
        listener.routine = 0;
        listener.param = 0;
        return 0;
    }
    if (listener.routine == 0) {
        listener.routine = routine;
        listener.param = param;
        return 0;
    }
    return -1;

}
