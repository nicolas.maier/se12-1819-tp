/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       27.03.2019
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <oled.h>
#include <font_8x8.h>

#include "draw.h"

#define INVERT_SCREEN true

// public methods implementation --------------------------------------------

void draw_init() {
    oled_init(OLED_V101);

    /*oled_memory_size(0, DRAW_DISPLAY_SIZE-1, 0, DRAW_DISPLAY_SIZE-1);
    struct pixel_b pixels_init[DRAW_DISPLAY_SIZE * DRAW_DISPLAY_SIZE];
    for (int i = 0; i < DRAW_DISPLAY_SIZE * DRAW_DISPLAY_SIZE; i++) {
        struct pixel_b p = {
            .hword = 0,
            .lword = 0
        };
        pixels_init[i] = p;
    }
    oled_send_image_b(pixels_init, DRAW_DISPLAY_SIZE * DRAW_DISPLAY_SIZE);


    int width = 40;
    int height = 40;

    oled_memory_size(0, height-1, 0, width-1);
    struct pixel_b pixels[width * height];
    int counter = 0;
    int color = 0;
    for (int i = 0; i < width * height; i++) {
        counter++;
        color = counter;
        if (counter > 31) {
            color |= 0b11111;
        }
        struct pixel_b p = {
            .hword = color >> 8,
            .lword = color & 0xff
        };
        pixels[i] = p;
    }
    oled_send_image_b(pixels, width * height);*/
}

void draw_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint16_t color) {
    // Inverser les coordonnées si besoin
    if (INVERT_SCREEN) {
        x = DRAW_DISPLAY_SIZE - x - w;
        y = DRAW_DISPLAY_SIZE - y - h;
    }

    // Préparer la mémoire
    oled_memory_size(
        DRAW_DISPLAY_SIZE - y - h,
        DRAW_DISPLAY_SIZE - 1 - y,
        DRAW_DISPLAY_SIZE - x - w,
        DRAW_DISPLAY_SIZE - 1 - x
    );

    // Remplir la liste des pixels à afficher
    struct pixel_b pixels[w * h];
    for (int i = w * h - 1; i >= 0; i--) {
        struct pixel_b p = {
            .hword = color >> 8,
            .lword = color & 0xff
        };
        pixels[i] = p;
    }

    // Envoyer les pixels vers l'écran
    oled_send_image_b(pixels, w*h);
}

void draw_circle(uint8_t cx, uint8_t cy, uint8_t r, uint16_t color) {
    // "Cercle" composé de plusieurs rectangles
    for (int i = -r; i <= r; i++) {
        for (int j = -r; j <= r; j++) {
            int w = round(sqrt(i*i + j*j));
            if (w <= r) {
                draw_rect(cx + i, cy + j, 1, 1, color);
            }
        }
    }
}

void draw_char(uint8_t x, uint8_t y, uint8_t c,
               uint16_t color, uint16_t bg_color) {
    // Inverser les coordonnées si besoin
    if (INVERT_SCREEN) {
        x = DRAW_DISPLAY_SIZE - x - DRAW_CHAR_SIZE;
        y = DRAW_DISPLAY_SIZE - y - DRAW_CHAR_SIZE;
    }

    // Préparer la mémoire
    oled_memory_size(
        DRAW_DISPLAY_SIZE - y - DRAW_CHAR_SIZE,
        DRAW_DISPLAY_SIZE - 1 - y,
        DRAW_DISPLAY_SIZE - x - DRAW_CHAR_SIZE,
        DRAW_DISPLAY_SIZE - 1 - x
    );

    // Remplir la liste des pixels à afficher
    struct pixel_b pixels[DRAW_CHAR_SIZE * DRAW_CHAR_SIZE];
    int pixelCount = INVERT_SCREEN ? DRAW_CHAR_SIZE * DRAW_CHAR_SIZE - 1 : 0;
    for (int col = 0; col < DRAW_CHAR_SIZE; col++) {
        for (int row = DRAW_CHAR_SIZE - 1; row >= 0; row--) {
            uint16_t pixelColor = fontdata_8x8[c][row] & 1<<col
                                ? color
                                : bg_color;
            struct pixel_b p = {
                .hword = pixelColor >> 8,
                .lword = pixelColor & 0xff
            };
            pixels[pixelCount] = p;
            pixelCount += INVERT_SCREEN ? -1 : 1;
        }
    }

    // Envoyer les pixels vers l'écran
    oled_send_image_b(pixels, DRAW_CHAR_SIZE * DRAW_CHAR_SIZE);
}

void draw_image(struct pixel_b* pixels, uint8_t x, uint8_t y,
        uint8_t w, uint8_t h) {
    // Inverser les coordonnées si besoin
    if (INVERT_SCREEN) {
        x = DRAW_DISPLAY_SIZE - x - w;
        y = DRAW_DISPLAY_SIZE - y - h;
    }

    // Préparer la mémoire
    oled_memory_size(
        DRAW_DISPLAY_SIZE - y - h,
        DRAW_DISPLAY_SIZE - 1 - y,
        DRAW_DISPLAY_SIZE - x - w,
        DRAW_DISPLAY_SIZE - 1 - x
    );

    // Envoyer les pixels vers l'écran
    oled_send_image_b(pixels, w*h);
}

void draw_move_element(uint8_t x, uint8_t y, uint8_t old_x, uint8_t old_y,
        uint8_t w, uint8_t h, uint16_t elt_color, uint16_t
        bg_color, int shrink) {
    int min_x = x < old_x ? x : old_x;
    int min_y = y < old_y ? y : old_y;
    int max_x = x > old_x ? x : old_x;
    int max_y = y > old_y ? y : old_y;
    int real_width = max_x - min_x + w;
    int real_height = max_y - min_y + h;

    struct pixel_b pixels[real_width * real_height];
    for (int col = 0; col < real_width; col++) {
        for (int row = 0; row < real_height; row++) {
            int color = row >= y-min_y && col-shrink >= x-min_x
                    && row < y-min_y + h && col+shrink < x-min_x + w
                    ? elt_color
                    : bg_color;

            struct pixel_b p = {
                .hword = color >> 8,
                .lword = color & 0xff
            };
            pixels[col * real_height + row] = p;
        }
    }

    draw_image(pixels, min_x, min_y, real_width, real_height);
}
