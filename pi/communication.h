/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       17.04.2019
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <stdint.h>
#include <stdbool.h>

#include "msgq.h"

enum communication_message {
    COMMUNICATION_HELLO, // Permet de synchroniser le démarrage
    COMMUNICATION_ACK, // Permet d'acquitter les messages reçus
    COMMUNICATION_BAR, // Permet de donner la position de la barre
    COMMUNICATION_BALL, // Permet de donner la position/direction de la balle
    COMMUNICATION_BOUNCE, // Permet d'indiquer que la balle rebondit
    COMMUNICATION_POINT, // Permet d'indiquer qu'un point a été gagné
};

// Initialise le thread de réception
void communication_init();
// Démarre la connexion (échange de messages "hello")
bool communication_start(bool isJ1, msgqid_t msgq_game,
        msgqid_t msgq_game_pty);
// Envoie la position de la barre
void communication_sendBar(uint8_t position);
// Envoie la position et la direction de la balle
bool communication_sendBall(uint8_t position_x, uint8_t position_y,
        float dir_x, float dir_y);
// Envoie un des événements, avec un paramètre
bool communication_sendEvent(enum communication_message message,
        uint8_t param);

#endif /* COMMUNICATION_H_ */
