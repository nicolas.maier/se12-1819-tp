/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       04.06.2019
 */

#include "stdint.h"
#include "stdbool.h"

#include "menu.h"
#include "communication.h"
#include "game.h"
#include "buttons.h"
#include "display.h"
#include "draw.h"

#define TEXT_1_POS_Y 23
#define TEXT_2_POS_Y 28
#define TEXT_OFFSET_Y 10

void menu_init(msgqid_t msgq_game, msgqid_t msgq_game_pty) {
    while (true) {
        // Demander au joueur de choisir "1" ou "2"
        display_clear_game_area();
        display_reset_fields();

        display_show_text(" Press ", DRAW_CHAR_SIZE, TEXT_1_POS_Y);
        display_show_text(" 1 or 2 ", DRAW_CHAR_SIZE/2,
                TEXT_1_POS_Y + TEXT_OFFSET_Y);
        display_show_text("to start", DRAW_CHAR_SIZE/2,
                TEXT_1_POS_Y + 2*TEXT_OFFSET_Y);

        bool isJ1 = false;
        while (true) {
            if (buttons_get_state(BUTTONS_1) == BUTTONS_CLOSED) {
                isJ1 = true;
                break;
            }
            if (buttons_get_state(BUTTONS_2) == BUTTONS_CLOSED) {
                isJ1 = false;
                break;
            }
        }

        // Attendre que l'autre joueur soit prêt
        display_clear_game_area();

        display_show_text(" Waiting ", 0, TEXT_2_POS_Y);
        display_show_text(isJ1 ? " for J2" : " for J1", DRAW_CHAR_SIZE/2,
                TEXT_2_POS_Y + TEXT_OFFSET_Y);

        if (!communication_start(isJ1, msgq_game, msgq_game_pty)) {
            game_start(isJ1, msgq_game, msgq_game_pty);
            break;
        }
    }
}

bool menu_should_stop() {
    return buttons_get_state(BUTTONS_4) == BUTTONS_CLOSED;
}
