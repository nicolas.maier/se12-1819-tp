/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems Laboratory
 *
 * Abstract: Basic Virtual File System Services
 *
 * Purpose: This module implements basic services of the device file system
 *
 * Author:  Daniel Gachet
 * Date:    08.05.2019
 */
#include <am335x_gpio.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "ff.h"

#include "vfs.h"

//#define printd(args...) printf (args)
#define printd(args...)

// --- file services ----------------------------------------------------------

struct file {
    FIL fil;
    char name[];
};

static int sd_open(struct vfs_file* file, int flags, int m)
{
    (void)m;
    BYTE mode  = 0;
    int access = flags & 0x3;

    if (access == O_RDONLY)
        mode |= FA_READ;
    else if (access == O_WRONLY)
        mode |= FA_WRITE;
    else if (access == O_RDWR)
        mode |= FA_READ | FA_WRITE;

    if (flags & O_APPEND)
        mode |= FA_OPEN_APPEND;
    else if (flags & O_TRUNC)
        mode |= FA_CREATE_ALWAYS;
    else if (flags & O_CREAT)
        mode |= FA_CREATE_NEW;

    printd("sd_open: file=%s, flags=%x, mode=%x\n", file->name, flags, mode);

    struct file* f = calloc(1, sizeof(*f) + strlen(file->name) + 1);
    if (f != 0) {
        strcpy(f->name, file->name);
        FRESULT r = f_open(&(f->fil), file->name, mode);
        if (r != FR_OK) {
            printd("sd: error opening file\n");
            free(f);
            f = 0;
        }
    }

    return (f == 0) ? -1 : (int)f;
}

static int sd_stat(struct vfs_dir* d, const char* name, struct stat* st)
{
    (void)d;
    memset(st, 0, sizeof(*st));
    if ((strlen(name) == 0) || (strcmp(name, "/") == 0)) {
        st->st_mode = S_IFDIR;
        st->st_mode |= S_IRWXU | S_IRWXG | S_IRWXO;
        struct tm tm = {
            .tm_year = 48,
            .tm_mon  = 1,
            .tm_mday = 1,
        };
        st->st_mtime = mktime(&tm);
        return 0;
    }
    FILINFO fno;
    FRESULT r = f_stat(name, &fno);
    if (r == 0) {
        if (fno.fattrib & AM_DIR)
            st->st_mode = S_IFDIR;
        else
            st->st_mode = S_IFREG;
        st->st_mode |= S_IRWXU | S_IRWXG | S_IRWXO;
        st->st_size  = fno.fsize;
        struct tm tm = {
            .tm_year = ((fno.fdate >> 9) + 10),
            .tm_mon  = ((fno.fdate >> 5) & 15),
            .tm_mday = ((fno.fdate >> 0) & 31),
            .tm_hour = ((fno.ftime >> 11) & 31),
            .tm_min  = ((fno.ftime >> 5) & 63),
            .tm_sec  = ((fno.ftime >> 0) & 31) * 2,
        };
        st->st_mtime = mktime(&tm);
    }
    return (r == 0) ? 0 : -1;
}

static int sd_read(struct vfs_file* f, char* buff, int len)
{
    UINT lr   = 0;
    FRESULT r = f_read(&(*(struct file*)f->id).fil, buff, len, &lr);
    printd("sd_read: %d/%d\n", r, lr);
    return (r != 0) ? -1 : (int)lr;
}

static int sd_write(struct vfs_file* f, const char* buff, int len)
{
    UINT lw   = 0;
    FRESULT r = f_write(&(*(struct file*)f->id).fil, buff, len, &lw);
    return (r != 0) ? -1 : (int)lw;
}

static int sd_lseek(struct vfs_file* f, int ptr, int dir)
{
    FSIZE_t ofs = ptr;

    switch (dir) {
        case 0 /*SEEK_SET*/:
            break;

        case 1 /*SEEK_CUR*/:
            ofs = f_tell(&(*(struct file*)f->id).fil) + ptr;
            break;

        case 2 /*SEEK_END*/:
            ofs = f_size(&(*(struct file*)f->id).fil) + ptr;
            break;
        default:
            return -1;
    }

    FRESULT r = f_lseek(&(*(struct file*)f->id).fil, ofs);
    if (r != 0) ofs = -1;

    return ofs;
}

static int sd_isatty(struct vfs_file* f)
{
    (void)f;
    return -1;
}

static int sd_fstat(struct vfs_file* f, struct stat* st)
{
    return sd_stat(0, (*(struct file*)f->id).name, st);
}

static int sd_close(struct vfs_file* f)
{
    FRESULT r = f_close(&(*(struct file*)f->id).fil);
    free((void*)f->id);
    return (r == 0) ? 0 : -1;
}

// --- directory services -----------------------------------------------------

struct directory {
    DIR directory;
    struct dirent entry;
};

static dir_t* sd_opendir(struct vfs_dir* d, const char* name)
{
    (void)d;
    dir_t* dir = calloc(1, sizeof(*dir));

    FRESULT res = f_opendir(&(dir->directory), name);

    if (res != FR_OK) {
        free(dir);
        return 0;
    }

    return dir;
}

static struct dirent* sd_readdir(struct vfs_dir* d, dir_t* dirp)
{
    (void)d;
    FILINFO fno;
    FRESULT res = f_readdir(&(dirp->directory), &fno);
    if ((res != FR_OK) || (fno.fname[0] == 0)) return 0;

    struct dirent* entry = &(dirp->entry);
    memset(entry, 0, sizeof(*entry));
    entry->d_ino  = 0;
    entry->d_type = (fno.fattrib & AM_DIR) != 0 ? DT_DIR : DT_REG;
    strncpy(entry->d_name, fno.fname, sizeof(entry->d_name) - 1);

    return entry;
}

static int sd_closedir(struct vfs_dir* d, dir_t* dirp)
{
    (void)d;
    free(dirp);
    return 0;
}

static int sd_mkdir(struct vfs_dir* d, const char* path, mode_t mode)
{
    (void)d;
    (void)mode;
    FRESULT res = f_mkdir(path);
    return (res != 0) ? -1 : 0;
}

static int sd_rmdir(struct vfs_dir* d, const char* path)
{
    (void)d;
    int status = 0;
    if (f_unlink(path) != 0) {
        status = -1;
    }
    printd("sd_rm: path=%s, status=%x\n", path, status);

    return status;
}

// --- internal structure -----------------------------------------------------

static struct vfs_file_ops fops = {
    .open   = sd_open,
    .read   = sd_read,
    .write  = sd_write,
    .lseek  = sd_lseek,
    .isatty = sd_isatty,
    .fstat  = sd_fstat,
    .close  = sd_close,
};

static struct vfs_dir_ops dops = {
    .opendir  = sd_opendir,
    .readdir  = sd_readdir,
    .closedir = sd_closedir,
    .stat     = sd_stat,
    .mkdir    = sd_mkdir,
    .rmdir    = sd_rmdir,
};

static FATFS fs;

// --- vfs public methods -----------------------------------------------------

static struct vfs_dir sdcard_get_dir(const char* name)
{
    struct vfs_dir dir = {
        .dops = &dops,
        .name = name,
    };
    return dir;
}

static struct vfs_file sdcard_get_file(const char* name)
{
    struct vfs_file file = {
        .fops = &fops,
        .name = name,
    };
    return file;
}

// --- public methods ---------------------------------------------------------

void sdcardfs_init()
{
    static bool is_initialized = false;

    vfs_set_method_get_dir(sdcard_get_dir);
    vfs_set_method_get_file(sdcard_get_file);

    am335x_gpio_setup_pin_in(0, 6, AM335X_GPIO_PULL_NONE, false);
    if (am335x_gpio_get_state(0, 6)) {
        printd("no SD-Card detected\n");
    } else {
        printd("SD-Card dected\n");
    }

    if (!is_initialized) {
        FRESULT r = f_mount(&fs, "/", 1);
        printd("sd_init: r=%d\n", r);
        if (r != 0) return;

        is_initialized = true;
    }
}
