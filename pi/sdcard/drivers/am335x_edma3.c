/**
 * Copyright 2017 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project: HEIA-FR / Embedded Systems Laboratory
 *
 * Abstract: AM335x EDMA3 Driver
 *
 * Purpose: This module implements basic services to drive the AM335x
 *          EDMA3 module.
 *
 * Author:  Romain Plouzin / Daniel Gachet
 * Date:    03.01.2017
 */

#include <am335x_clock.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "am335x_edma3.h"

// -----------------------------------------------------------------------------
// global macros and defintions
// -----------------------------------------------------------------------------
#define REGION_ID (0)

#define DMAQNUM_CLR(channel) (~(0x7 << (((channel) % 8) * 4)))
#define DMAQNUM_SET(channel, queue) ((0x7 & (queue)) << (((channel) % 8) * 4))
#define QDMAQNUM_SET(channel, queue) ((0x7 & (queue)) << ((channel)*4))
#define OPT_TCC_CLR (~AM335X_EDMA3_OPT_TCC_MASK)
#define OPT_TCC_SET(channel) ((channel) << AM335X_EDMA3_OPT_TCC_SHIFT)

#define SET_ALL_BITS (0xFFFFFFFFu)

#define COMPL_HANDLER_RETRY_COUNT (10)
#define ERR_HANDLER_RETRY_COUNT (10)

#define EDMA3_NUM_DMACH (64)
#define EDMA3_NUM_QDMACH (8)
#define EDMA3_NUM_EVQUE (4)
#define EDMA3_NUM_TCC (64)

#define CCERR_TCCERR_SHIFT (0x00000010)

struct am335x_edma_shadow_region {
    uint32_t er[2];     // 0-4
    uint32_t ecr[2];    // 8-c
    uint32_t esr[2];    // 10-14
    uint32_t cer[2];    // 18-1c
    uint32_t eer[2];    // 20-24
    uint32_t eecr[2];   // 28-2c
    uint32_t eesr[2];   // 30-34
    uint32_t ser[2];    // 38-3c
    uint32_t secr[2];   // 40-44
    uint32_t res0[2];   // 48-4c (reserved)
    uint32_t ier[2];    // 50-54
    uint32_t iecr[2];   // 58-5c
    uint32_t iesr[2];   // 60-64
    uint32_t ipr[2];    // 68-6c
    uint32_t icr[2];    // 70-74
    uint32_t res1[2];   // 78-7c
    uint32_t qer;       // 80
    uint32_t qeer;      // 84
    uint32_t qeecr;     // 88
    uint32_t qeesr;     // 8c
    uint32_t qser;      // 90
    uint32_t qsecr;     // 94
    uint32_t res2[90];  // 98 - 1fc (reserved)
};

static volatile struct am335x_edma_ctrl {
    uint32_t pid;                                // 0
    uint32_t cccfg;                              // 4
    uint32_t res0[2];                            // 8 - c        (reserved)
    uint32_t sysconfig;                          // 10
    uint32_t res1[59];                           // 14 - fc      (reserved)
    uint32_t dchmap[64];                         // 100 - 1fc
    uint32_t qchmap[8];                          // 200 - 21c
    uint32_t res2[8];                            // 220 - 23c        (reserved)
    uint32_t dmaqnum[8];                         // 240 - 25c
    uint32_t qdmaqnum;                           // 260
    uint32_t res3[8];                            // 264 - 280        (reserved)
    uint32_t quepri;                             // 284
    uint32_t res4[30];                           // 288 - 2fc        (reserved)
    uint32_t emr[2];                             // 300 - 304
    uint32_t emcr[2];                            // 308 - 30c
    uint32_t qemr;                               // 310
    uint32_t qemcr;                              // 314
    uint32_t ccerr;                              // 318
    uint32_t ccerrclr;                           // 31c
    uint32_t eeval;                              // 320
    uint32_t res5[7];                            // 324 - 33c        (reserved)
    uint32_t drae0[2];                           // 340 - 344
    uint32_t drae1[2];                           // 348 - 34c
    uint32_t drae2[2];                           // 350 - 354
    uint32_t drae3[2];                           // 358 - 35c
    uint32_t drae4[2];                           // 360 - 364
    uint32_t drae5[2];                           // 368 - 36c
    uint32_t drae6[2];                           // 370 - 374
    uint32_t drae7[2];                           // 378 - 37c
    uint32_t qrae[8];                            // 380 - 398
    uint32_t res19[24];                          // 39c - 3fc
    uint32_t q0e[16];                            // 400 - 43c
    uint32_t q1e[16];                            // 440 - 47c
    uint32_t q2e[16];                            // 480 - 4bc
    uint32_t res6[80];                           // 4c0 - 600    (reserved)
    uint32_t qstat[3];                           // 600 - 608
    uint32_t res7[5];                            // 60c - 61c    (reserved)
    uint32_t qwmthra;                            // 620
    uint32_t res8[7];                            // 624 - 63c
    uint32_t ccstat;                             // 640
    uint32_t res9[111];                          // 644 - 7fc    (reserved)
    uint32_t mpfar;                              // 800
    uint32_t mpfsr;                              // 804
    uint32_t mpfcr;                              // 808
    uint32_t mppag;                              // 80c
    uint32_t mppa[8];                            // 810 - 82c
    uint32_t res10[500];                         // 830 - ffc    (reserved)
    uint32_t er[2];                              // 1000-1004
    uint32_t res11[2];                           // 1008-100c    (reserved)
    uint32_t esr[2];                             // 1010-1014
    uint32_t cer[2];                             // 1018-101c
    uint32_t eer[2];                             // 1020-1024
    uint32_t eecr[2];                            // 1028-102c
    uint32_t eesr[2];                            // 1030-1034
    uint32_t ser[2];                             // 1038-103c
    uint32_t res12[4];                           // 1040-104c    (reserved)
    uint32_t ier[2];                             // 1050-1054
    uint32_t iecr[2];                            // 1058-105c
    uint32_t iesr[2];                            // 1060-1064
    uint32_t ipr[2];                             // 1068-106c
    uint32_t icr[2];                             // 1070-1074
    uint32_t ieval;                              // 1078
    uint32_t res13[1];                           // 107c     (reserved)
    uint32_t qer;                                // 1080
    uint32_t qeer;                               // 1084
    uint32_t qeecr;                              // 1088
    uint32_t qeesr;                              // 108c
    uint32_t qser;                               // 1090
    uint32_t qsecr;                              // 1094
    uint32_t res14[986];                         // 1098-1ffc    (reserved)
    struct am335x_edma_shadow_region shadow[8];  // 2000 - 2ffc
    uint32_t res18[1024];                        // 3000 - 3ffc  (reserved)
    struct am335x_edma3_param PaRAM[256];        // 4000 - 5ffc
}* edmacc = (struct am335x_edma_ctrl*)(0x49000000);

// EDMA callback function array
static struct cb_handler {
    void (*routine)(void* param);
    void* param;
} callback[EDMA3_NUM_TCC];

// -----------------------------------------------------------------------------
// internal methods
// -----------------------------------------------------------------------------

static void am335x_edma_enable_dma_event(uint32_t channel)
{
    edmacc->shadow[REGION_ID].eesr[channel / 32] |= (0x01u << (channel % 32));
}

// -----------------------------------------------------------------------------

static void am335x_edma3_clear_missed_event(uint32_t channel)
{
    edmacc->shadow[REGION_ID].secr[channel / 32] = (0x01u << (channel % 32));
    edmacc->emcr[channel / 32] |= (0x01u << (channel % 32));
}

// -----------------------------------------------------------------------------

static int am335x_edma_disable_transfer(uint32_t channel,
                                        enum am335x_edma3_trigger_mode mode)
{
    int status = -1;
    switch (mode) {
        case AM335X_EDMA3_TRIG_MODE_MANUAL:
            if (channel < EDMA3_NUM_DMACH) {
                edmacc->shadow[REGION_ID].ecr[channel / 32] |=
                    (0x01 << (channel % 32));
                status = 0;
            }
            break;

        case AM335X_EDMA3_TRIG_MODE_QDMA:
            if (channel < EDMA3_NUM_QDMACH) {
                edmacc->shadow[REGION_ID].qeecr = (0x01 << channel);
                status                          = 0;
            }
            break;

        case AM335X_EDMA3_TRIG_MODE_EVENT:
            if (channel < EDMA3_NUM_DMACH) {
                // clear SECR & EMCR to clean any previous NULL request
                am335x_edma3_clear_missed_event(channel);

                // Set EECR to enable event
                edmacc->shadow[REGION_ID].eecr[channel / 32] |=
                    (0x01 << (channel % 32));
                status = 0;
            }
            break;
    }
    return status;
}

// -----------------------------------------------------------------------------
// public method implementation
// -----------------------------------------------------------------------------

void am335x_edma3_init(uint32_t queue)
{
    // enable edma3 clock module
    am335x_clock_enable_edma_module();

    // Clear the Event miss Registers
    edmacc->emcr[0] = SET_ALL_BITS;
    edmacc->emcr[1] = SET_ALL_BITS;
    edmacc->qemcr   = SET_ALL_BITS;

    // Clear CCERR register
    edmacc->ccerrclr = SET_ALL_BITS;

    // FOR TYPE EDMA
    // Enable the DMA (0 - 64) channels in the DRAE and DRAEH registers
    edmacc->drae0[0] = SET_ALL_BITS;
    edmacc->drae0[1] = SET_ALL_BITS;

    for (unsigned i = 0; i < 64; i++) {
        // All events are one to one mapped with the channels
        edmacc->dchmap[i] = i << 5;
    }

    // Initialize the DMA Queue Number Registers
    for (unsigned i = 0; i < EDMA3_NUM_DMACH; i++) {
        edmacc->dmaqnum[i / 8] &= ~(0x7 << ((i % 8) * 4));
        edmacc->dmaqnum[i / 8] |= (0x7 & queue) << ((i % 8) * 4);
    }

    // Enable the DMA (0 - 63) channels in the QRAE register
    edmacc->qrae[REGION_ID] = 0xff;

    // Initialize the QDMA Queue Number Registers
    for (unsigned i = 0; i < EDMA3_NUM_QDMACH; i++) {
        edmacc->qdmaqnum &= ~(0x7 << (i * 4));
        edmacc->qdmaqnum |= (0x7 & queue) << (i * 4);
    }
}

// -----------------------------------------------------------------------------

int am335x_edma3_channel_init(enum am335x_edma3_channel_type type,
                              uint32_t channel,
                              uint32_t queue,
                              void (*routine)(void* param),
                              void* param)
{
    int status = -1;

    // Registering Callback Function for TX
    callback[channel].routine = routine;
    callback[channel].param   = param;

    // Request DMA Channel and TCC for MMCSD Transmit
    switch (type) {
        case AM335X_EDMA3_CHANNEL_TYPE_DMA:
            if (channel < EDMA3_NUM_DMACH) {
                // Enable the DMA channel in the DRAE registers
                edmacc->drae0[channel / 32] |= (0x01u << (channel % 32));

                // Associate DMA Channel to Event Queue
                edmacc->dmaqnum[channel >> 3u] &= DMAQNUM_CLR(channel);
                edmacc->dmaqnum[channel >> 3u] |= DMAQNUM_SET((channel), queue);
                status = 0;
            }
            break;

        case AM335X_EDMA3_CHANNEL_TYPE_QDMA:
            if (channel < EDMA3_NUM_QDMACH) {
                // Enable the QDMA channel in the DRAE/DRAEH registers
                edmacc->qrae[REGION_ID] |= 0x01u << channel;

                // Associate QDMA Channel to Event Queue
                edmacc->qdmaqnum |= QDMAQNUM_SET(channel, queue);
                status = 0;
            }
            break;
    }
    if (status == 0) {
        // Enable the Event Interrupt
        edmacc->shadow[REGION_ID].iesr[channel / 32] |=
            (0x01 << (channel % 32));

        edmacc->PaRAM[channel].opt &= OPT_TCC_CLR;
        edmacc->PaRAM[channel].opt |= OPT_TCC_SET(channel);
    }
    return status;
}

// -----------------------------------------------------------------------------

void am335x_edma3_set_param(uint32_t channel, struct am335x_edma3_param* param)
{
    edmacc->PaRAM[channel] = *param;
}

// -----------------------------------------------------------------------------

int am335x_edma3_enable_transfer(uint32_t channel,
                                 enum am335x_edma3_trigger_mode mode)
{
    int status = -1;
    switch (mode) {
        case AM335X_EDMA3_TRIG_MODE_MANUAL:
            if (channel < EDMA3_NUM_DMACH) {
                edmacc->shadow[REGION_ID].esr[channel / 32] |=
                    (0x01u << (channel % 32));
                status = 0;
            }
            break;

        case AM335X_EDMA3_TRIG_MODE_QDMA:
            if (channel < EDMA3_NUM_QDMACH) {
                edmacc->shadow[REGION_ID].qeesr = (0x01u << channel);
                status                          = 0;
            }
            break;

        case AM335X_EDMA3_TRIG_MODE_EVENT:
            if (channel < EDMA3_NUM_DMACH) {
                // clear SECR & EMCR to clean any previous NULL request
                am335x_edma3_clear_missed_event(channel);

                // Set EESR to enable event
                am335x_edma_enable_dma_event(channel);
                status = 0;
            }
            break;
    }
    return status;
}

// -----------------------------------------------------------------------------

void am335x_edma3_completion_isr()
{
    uint32_t count = 0;
    while (true) {
        if (count >= COMPL_HANDLER_RETRY_COUNT) break;

        uint32_t iprl = edmacc->shadow[REGION_ID].ipr[0];
        uint32_t iprh = edmacc->shadow[REGION_ID].ipr[1];
        if ((iprl | iprh) == 0) break;

        uint32_t channel = 0;
        uint32_t ipr     = iprl;
        for (int i = 0; i < 2; i++) {
            while (ipr != 0) {
                if ((ipr & 0x1) != 0) {
                    // here write to ICR to clear the corresponding IPR bits
                    edmacc->shadow[REGION_ID].icr[channel / 32] =
                        (1u << (channel % 32));
                    am335x_edma_disable_transfer(channel,
                                                 AM335X_EDMA3_TRIG_MODE_EVENT);
                    if (callback[channel].routine != 0) {
                        callback[channel].routine(callback[channel].param);
                    }
                }
                channel++;
                ipr >>= 1;
            }
            channel = 32;
            ipr     = iprh;
        }
        count++;
    }
}

// -----------------------------------------------------------------------------

void am335x_edma3_error_isr()
{
    uint32_t iprl = edmacc->shadow[REGION_ID].ipr[0];
    (void)iprl;
    uint32_t iprh = edmacc->shadow[REGION_ID].ipr[1];
    (void)iprh;

    uint32_t count = 0;
    while (true) {
        if (count >= ERR_HANDLER_RETRY_COUNT) break;

        uint32_t emrl  = edmacc->emr[0];
        uint32_t emrh  = edmacc->emr[1];
        uint32_t qemr  = edmacc->qemr;
        uint32_t ccerr = edmacc->ccerr;
        if ((emrl | emrh | qemr | ccerr) == 0) break;

        // clear all DMA missed events
        if (emrl != 0) {
            edmacc->shadow[REGION_ID].secr[0] = emrl;
            edmacc->emcr[0]                   = emrl;
        }
        if (emrh != 0) {
            edmacc->shadow[REGION_ID].secr[1] = emrh;
            edmacc->emcr[1]                   = emrh;
        }

        // clear all QDMA missed events
        if (qemr != 0) {
            edmacc->shadow[REGION_ID].qsecr = qemr;
            edmacc->qemcr                   = qemr;
        }

        // clear all pending queue threshold and transfer completion errors
        if (ccerr != 0) {
            edmacc->ccerrclr = ccerr;
        }
        count++;
    }
}
