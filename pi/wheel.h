/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       13.03.2019
 */

#pragma once
#ifndef WHEEL_H
#define WHEEL_H

// initialize GPIOs for the wheel
//void wheel_init();

// returns the current direction of the wheel
enum wheel_direction wheel_get_direction();

/**
 * wheel rotation directions
 */
enum wheel_direction {
    WHEEL_STILL, WHEEL_RIGHT, WHEEL_LEFT
};

typedef void (*wheel_handler_t)(enum wheel_direction direction, void* param);

/**
 * method to initialize the resoures of the wheel
 * this method shall be called prior any other.
 */
extern void wheel_init();

/**
 * method to hook and unhook a wheel service routine to a specified
 * interrupt source
 *
 * @param routine wheel service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int wheel_on_event(wheel_handler_t routine, void* param);

#endif
