# PI.4 : Infos sur les Makefiles, les pilotes pour les périphériques OLED, nRF24 et RFid, le noyau et la libbbb

## Makefiles

La suite de Makefiles faisant partie de la bibliothèque _libbbb_ et utilisée pour la génération des applications permet une structuration des fichiers sources sur 2 niveaux, p.ex.

```
./pi                           <-- 1r niveau : projet intégré
   Makefile
   "sources du projet"
   ./kernel                    <-- 2e niveau : library kernel
      Makefile
      "source du noyau"
   ./nrf                       <-- 2e niveau: library nrf, pilote de la carte nRF24
      Makefile
      "source du pilote nrf"
   ./rfid                      <-- 2e niveau : library rfid, pilote de la carte RFid
      Makefile
      "source du pilote rfid"
```
Afin que la génération de l'application se passe correctement, le Makefile du 1er niveau devra contenir la liste des dossiers (bibliothèques) de 2e niveau (ici: kernel, nrf et rfid). Celles-ci s'indiquent avec la variable _`LIBDIRS`_ comme présenté ci-dessous :

```Makefile
EXEC=app
SRCS=$(wildcard *.c)
ASRC=$(wildcard *.S)

LIBDIRS+=kernel nrf
LIBDIRS+=rfid

include $(LMIBASE)/bbb/make/bbb.mk
```

Il est intéressant de noter qu'il est possible d'énumérer les éléments de cette liste sur plusieurs lignes.

La génération des bibliothèques du 2e niveau passe également par un Makefile. Afin qu'ils génèrent une _library_ et non pas une application, l'appel au Makefile de la _libbbb_ devra s'effectuer avec le fichicher _`bbb_lib.mk`_. Si la bibliothèque fait usage de ".h-files" d'autres bibliothèques, il est possible de l'indiquer avec la variable _`CFLAGS`_, comme suit _`CLFAGS+=-I../libname`_ . Voici un exemple de Makefile de 2e niveau :

```Makefile
SRCS=$(wildcard *.c)
ASRC=$(wildcard *.S)

CFLAGS+=-I../kernel

include $(LMIBASE)/bbb/make/bbb_lib.mk
```

Ceux-ci sont appelés automatique par le Makefile du 1er niveau. Bien qu'il soit possible d'appeler chaque Makefile de manière indépendante, il n'est pas nécessaire de le faire manuellement. Le Makefile du 1er niveau le fera lors de son appel.


## Pilotes et connexions

Les bibliothèques ci-dessous permettent le pilotage des différentes _Click Board_ mises à disposition pour le projet. Il faut noter que ces cartes ne peuvent être connectées à n'importe quelle _cape_ de la carte d'extension du Beaglebone Black, car celles-ci ont des signaux hardware bien précis et ne peuvent pas être déplacées sans adaptation de leur pilote. Il sera donc important de bien respecter les indications ci-dessous.

### OLED

La carte _OLED_ doit ête connectée dans la **CAPE 2**. Son pilote, ainsi que la police _font8x8.h_, se trouvent dans la bibliothéque _libbbb_. Il faudra par conséquent les effacer de vos répertoires. _OLED_ offre maintenant la méthode _`oled_send_image_b()`_ pour l'envoi d'images avec des pixels représentés par 2 octets au lieu de 2 mots de 4 octets. Ce format est plus performant et plus simple à utiliser.

Pour son utilisation avec les autres cartes citées ci-dessous, l'_OLED_ doit impérativement être modifiée. Si ceci n'a pas encore été effectué, il faut l'apporter au professeur qui la fera modifier.

_A noter, qu'il faut continuer à indiduer la version de la carte OLED dans le Makefile   
(`CFLAGS+=-D OLED_VERSION=OLED_V101`)._

### nRF24

La carte _nRF24_ peut ête connectée aussi bien dans la **CAPE 1** que dans la **CAPE 2**, par défaut le pilote choisit la _CAPE 1_. Cependant, si l'application exige de connecter la carte dans la _CAPE 2_, la méthode _`nrf24_select_cape_2()`_ permettra d'informer le pilote de ce choix. Cette méthode doit impérativement être appelée avant tout autre méthode du pilote.

### RFid

La carte _RFID_ doit **impérativement** ête connectée dans la **CAPE 1**.

## Noyau

Le noyau développé dans la série d'exercices du cours est maintenant à disposition sous forme de _library_.

## libbb

Le développement des différents pilotes ci-dessus a exigé des adaptations de la bibliothèque _`libbbb`_. Celles-ci sont maintenant disponibles dans le dépôt, mais pour en profiter il faut impérativement effectuer les opérations suivantes:

```bash
$ cd ~/workspace/se12/tp
$ git pull upstream master
$ make -C ./bbb/source
```

Ces opérations doivent être effectuées sur chaque ordinateur utilisé pour le développement logiciel du projet intégré.