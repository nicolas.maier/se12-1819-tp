#pragma once
#ifndef MENU_H
#define MENU_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 2
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       04.06.2019
 */

#include "msgq.h"

// Affiche/retourne au menu
extern void menu_init(msgqid_t msgq_game, msgqid_t msgq_game_pty);
// Indique si le joueur souhaite retourner au menu
extern bool menu_should_stop();

#endif
