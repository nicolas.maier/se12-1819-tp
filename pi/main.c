/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:             HEIA-FR / Embedded Systems 2
 *
 *
 * Author:              Nicolas Maier & Jonathan Dias Vicente / HEIA-FR
 * Date:                17.04.2019
 */

#include <stdio.h>
#include <string.h>

#include <am335x_gpio.h>
#include <stdint.h>
#include <stdbool.h>
#include "interrupt.h"
#include "communication.h"
#include "exception.h"
#include "intc.h"
#include "menu.h"
#include "wheel.h"
#include "leds.h"
#include "gpio.h"
#include "display.h"
#include "game.h"
#include "timer.h"
#include "buttons.h"

#include "kernel.h"
#include "thread.h"
#include "msgq.h"
#include "nrf24.h"

int main() {
    // Initialisation des différents modules
    interrupt_init();
    exception_init();
    intc_init();

    kernel_init();
    thread_init();

    gpio_init();
    wheel_init();
    buttons_init();
    leds_init();
    display_init();

    timer_init(DMTIMER_2);
    timer_init(DMTIMER_3);
    timer_init(DMTIMER_4);
    timer_init(DMTIMER_5);

    communication_init();
    game_init();

    // Création des files de messages
    msgqid_t msgq_game = msgq_create(50);
    msgqid_t msgq_game_pty = msgq_create(50);

    // Démarrage du menu
    menu_init(msgq_game, msgq_game_pty);

    // Démarrage du "kernel" (lancement des "threads")
    kernel_launch();
}
