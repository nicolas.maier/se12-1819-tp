/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:    HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Author:     Nicolas Maier - Jonathan Dias Vicente
 * Date:       02.10.2018
 */

#include <stdbool.h>
#include <stdint.h>
#include <am335x_gpio.h>
#include "leds.h"

#define GPIO1 AM335X_GPIO1

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// local variables ----------------------------------------------

static const struct gpio_init {
    uint8_t pin_nr;
} gpio_init[] = {
        { 12 }, // LED 1
        { 13 }, // LED 2
        { 14 }, // LED 3
    };

// public methods implementation ------------------------------

void leds_init() {
    // initialize gpio module
    am335x_gpio_init(GPIO1);

    // configure gpio pins as output
    for (int8_t i = ARRAY_SIZE(gpio_init) - 1; i >= 0; i--) {
        am335x_gpio_setup_pin_out(GPIO1, gpio_init[i].pin_nr, false);
    }
}

void leds_set_state(uint16_t leds) {
    am335x_gpio_change_states(GPIO1, 0xFFFFFFFF, false);
    am335x_gpio_change_states(GPIO1, leds, true);
}
