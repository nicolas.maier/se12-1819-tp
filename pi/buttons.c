/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract: Switches Device Driver
 *
 * Purpose:     This module implements a method to get state of the switches
 *                      of the HEIA-FR extension board of the Beaglebone black.
 *
 * Author:      Nicolas Maier / Jonathan dias Vicente
 * Date:        03.06.2018
 */

#include <am335x_gpio.h>
#include <stdbool.h>
#include "buttons.h"
#include "gpio.h"

// i/o pin definition --------------------------------------------------------

#define SW_GPIO         AM335X_GPIO1
#define SW_GPIO_4       AM335X_GPIO2
#define S1_PIN          15
#define S2_PIN          16
#define S3_PIN          17
#define S4_PIN          8

#define FIRST_BUTTON    BUTTONS_1
#define NB_BUTTONS      BUTTONS_COUNT

static const uint32_t button[NB_BUTTONS] = {
        [BUTTONS_1] = S1_PIN,
        [BUTTONS_2] = S2_PIN,
        [BUTTONS_3] = S3_PIN,
        [BUTTONS_4] = S4_PIN,
};

static const enum buttons_states buttons_states[2][2] = {
        {BUTTONS_CLOSED,  BUTTONS_RELEASED},
        {BUTTONS_PRESSED, BUTTONS_OPEN    },
};

static bool former_state[NB_BUTTONS];

static struct listener {
    buttons_handler_t routine;
    void* param;
} listener;

static void buttons_handler(void* param) {
    if (listener.routine != 0) {
        listener.routine((enum buttons_set) param, listener.param);
    }
}

// public method implementation ----------------------------------------------

void buttons_init()
{
    // initialize gpio module
    am335x_gpio_init(SW_GPIO);
    am335x_gpio_init(SW_GPIO_4);

    // configure gpio pins as input
    for (int i=0; i<NB_BUTTONS;i++) {
        am335x_gpio_setup_pin_in(i==BUTTONS_4 ? SW_GPIO_4 : SW_GPIO, button[i],
                AM335X_GPIO_PULL_NONE, true);

        former_state[i] = am335x_gpio_get_state(
                i==BUTTONS_4 ? SW_GPIO_4 : SW_GPIO, button[i]);

        gpio_on_event(i==BUTTONS_4 ? SW_GPIO_4 : SW_GPIO, button[i],
                    GPIO_FALLING | GPIO_DEBOUNCED, buttons_handler, (void*)i);
    }
}

enum buttons_states buttons_get_state(enum buttons_set btn)
{
        bool new_state  = am335x_gpio_get_state(
                btn==BUTTONS_4 ? SW_GPIO_4 : SW_GPIO, button[btn]);
        enum buttons_states state
                = buttons_states[former_state[btn]][new_state];
        former_state[btn] = new_state;
        return state;
}

int buttons_on_event(buttons_handler_t routine, void* param) {
    if (routine == 0) {
        listener.routine = 0;
        listener.param = 0;
        return 0;
    }
    if (listener.routine == 0) {
        listener.routine = routine;
        listener.param = param;
        return 0;
    }
    return -1;
}
