# Présentation des projets intégrés

## Format

* Présentation : 5 minutes
* Démonstration : 2 minutes
* Questions et réponses : 3 minutes
* Feedback du public : 5 minutes

Pendant le feedback du public, le groupe qui présentait prend note des remarques et le groupe suivant se prépare.

## Présentation

* [Présentations "Zen"](https://www.presentationzen.com/)
* Introduction / contexte
* Présentation de l'application
* Détails techniques
    * Utilisation des "threads" et des "queues"
    * Utilisation des interruptions
    * Utilisation (ou non) des variables globales
    * Algorithmes et structures de données utilisés
    * Prise en main et utilisation du module de communication (nRF ou RFID)
* Tests effectués
* Gestion du projet / planification
* Les hauts (satisfactions) et les bas (problèmes) du projet
* Conclusion

## Slots du 5 juin (groupe I2A)

| Slot | Heure | Groupe                                 |
| ---: | ----- | -------------------------------------- |
|  1   | 13:30 | Laurent Chassot et Tony Licata         |
|  2   | 13:50 | Emerald Cottet et Quentin Seydoux      |
|  3   | 14:05 | Loïc Amantini et Samuel Baula          |
|  4   | 14:20 | Mathieu Baechler et Vincent Brodard    |
|  5   | 15:00 | Nicolas Maier et Jonathan Dias Vicente |
|  6   | 15:15 | Amanda Hayoz et Kevin Pantillon        |
|  7   | 15:30 | Loïc Domeniconi et Stefan Pahud        |
|  8   | 15:50 | Sten Kapferer et Rémi Zamofing         |
|  9   | 16:05 | Martin Spoto et Victor Bernet          |
| 10   | 16:20 | -----                                  |

## Slots du 12 juin (groupe I2B)

| Slot | Heure | Groupe                                                  |
| ---: | ----- | ------------------------------------------------------- |
|  1   | 13:30 | Kevin Manixab et Luke Perrottet                         |
|  2   | 13:50 |Dorian Saudan et Uchendu Nwachukwu                       |
|  3   | 14:05 |Adrian Buntschu et Julien Härle                          |
|  4   | 14:20 |Yulia Termkhitarova et Samoelina Hana Nantenaina Ranaivo |
|  5   | 15:00 |Guillaume Dévaud et Nicolas Crausaz                      |
|  6   | 15:15 |Nicolas Realini et Daniel D'Incà                         |
|  7   | 15:30 |Yannis Huber et Nicolas Feyer                            |
|  8   | 15:50 |Mireille Favre-Bulle et Hindi Ollyn Vasco Golay          |
|  9   | 16:05 |Florent Kilchoer, Luca De Laurentiis et Lea Corsi        |
| 10   | 16:20 |Loïc Freiburghaus et Yael Iseli                          |

## Structure du rapport

* Introduction
* Analyse
* Spécification / Conception
    * Algorithmes
    * Structures de données
    * Pseudo-code
    * Classes
    * Threads
* Réalisation
    * Architecture
* Tests et validations
* Conclusions
